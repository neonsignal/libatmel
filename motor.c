// Copyright 2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// Stepper Motor Driver
// include files
#include <avr/interrupt.h>
#include "motor.h"

// motor states
enum // motor states
{
	SlewRampup, SlewSteady, SlewRampdown, SlewSlow,
#	ifdef SCAN
	PrescanRampup, PrescanSteady, PrescanRampdown,
	ScanRampup, ScanSteady, ScanRampdown,
#	endif
	Idle
};

enum // initial states
{
	Slew = SlewRampup,
#ifdef SCAN
	FastScan = PrescanRampup,
	SlowScan = ScanRampup,
#endif
};

// interrupt routine
void updateMotor(Motor *pMotor)
{
	bool stop;
	// output motor position
#	ifdef LITTLE_ENDIAN
	typedef struct
	{
		uint8_t nanostep;
		uint8_t ustep;
		uint16_t quadstep;
	} Position;
#	else
	typedef struct
	{
		uint16_t quadstep;
		uint8_t ustep;
		uint8_t nanostep;
	} Position;
#	endif
	stop = pMotor->outMotor(((Position *) &(pMotor->sCurrent))->ustep ^ pMotor->direction);
	stop &= pMotor->switchMask;
	stop ^= pMotor->switchPolarity;

	// calculate next position
	switch (pMotor->state)
	{

	// slew_rampup:
	case SlewRampup:
		pMotor->v += pMotor->a;
		if (stop || pMotor->sCurrent+pMotor->v > pMotor->sTarget-pMotor->v)
			goto slew_rampdown;
		if (pMotor->v >= pMotor->vMax)
			goto slew_steady;
		pMotor->sTarget -= pMotor->v;
		pMotor->sCurrent += pMotor->v;
		break;
	slew_steady:
		pMotor->state = SlewSteady;
	case SlewSteady:
		if (stop || pMotor->sCurrent+pMotor->vMax > pMotor->sTarget)
			goto slew_rampdown;
		pMotor->sCurrent += pMotor->vMax;
		break;
	slew_rampdown:
		pMotor->state = SlewRampdown;
	case SlewRampdown:
		if (stop || pMotor->sCurrent+pMotor->v > pMotor->sTarget)
		{
			pMotor->v -= pMotor->a;
			if (pMotor->v == 0)
				goto slew_slow;
			pMotor->sTarget += pMotor->v;
		}
		pMotor->sCurrent += pMotor->v;
		break;
	slew_slow:
		pMotor->state = SlewSlow;
	case SlewSlow:
		if (stop || pMotor->sCurrent == pMotor->sTarget)
			goto idle;
		pMotor->sCurrent = pMotor->sTarget;
		break;

#	ifdef SCAN
	// prescan_rampup:
	case PrescanRampup:
		pMotor->v += pMotor->a;
		if (stop)
			goto prescan_rampdown;
		pMotor->t = pMotor->t+1;
		if (pMotor->v+pMotor->v+pMotor->a >= pMotor->vMax)
		{
			if (pMotor->v+pMotor->v < pMotor->vMax)
				pMotor->t += 2;
			goto prescan_steady;
		}
		pMotor->sCurrent -= pMotor->v;
		break;
	prescan_steady:
		pMotor->state = PrescanSteady;
	case PrescanSteady:
		if (stop || pMotor->t == 0)
			goto prescan_rampdown;
		pMotor->sCurrent -= pMotor->v;
		--pMotor->t;
		break;
	prescan_rampdown:
		pMotor->state = PrescanRampdown;
	case PrescanRampdown:
		pMotor->v -= pMotor->a;
		if (pMotor->v == 0)
		{
			if (stop)
				goto idle;
			pMotor->state = ScanRampup;
			break;
		}
		pMotor->sCurrent -= pMotor->v;
		break;

	// scan_rampup:
	case ScanRampup:
		pMotor->v += pMotor->a;
		if (stop)
			goto scan_rampdown;
		if (pMotor->v >= pMotor->vMax)
			goto scan_steady;
		pMotor->sCurrent += pMotor->v;
		break;
	scan_steady:
		pMotor->state = ScanSteady;
	case ScanSteady:
		if (stop || pMotor->sCurrent >= pMotor->sTarget)
			goto scan_rampdown;
		pMotor->sCurrent += pMotor->vMax;
		break;
	scan_rampdown:
		pMotor->state = ScanRampdown;
	case ScanRampdown:
		pMotor->v -= pMotor->a;
		if (pMotor->v == 0)
			goto idle;
		pMotor->sCurrent += pMotor->v;
		pMotor->state = ScanRampdown;
		break;
#	endif
	idle:
		pMotor->state = Idle;
		pMotor->enableMotor(0);
		setInterruptSemaphore(&(pMotor->done));
	case Idle:
		break;
	}
}

static void resetMotor(Motor *pMotor, int32_t s, uint16_t v, uint8_t switchMask, uint8_t switchPolarity)
{
	// ensure the motor is stopped
	pMotor->switchMask = 0;
	pMotor->switchPolarity = -1;
	waitInterruptSemaphore(&(pMotor->done));

	// set new target
	s -= pMotor->sOffset;
	pMotor->sTarget = pMotor->direction==0x00 ? s : ~s;
	pMotor->vMax = v;
	pMotor->switchMask = switchMask;
	pMotor->switchPolarity = switchPolarity;
}

static void offsetMotor(Motor *pMotor)
{
#	ifdef LITTLE_ENDIAN
	typedef struct
	{
		uint16_t ustep;
		uint16_t quadstep;
	} Position;
#	else
	typedef struct
	{
		uint16_t quadstep;
		uint16_t ustep;
	} Position;
#	endif

	// discard quadstep position
	((Position *) &(pMotor->sCurrent))->quadstep = pMotor->direction==0x00 ? 0 : ~0;

	// retain step-microstep-nanostep offset
	pMotor->sOffset = pMotor->direction==0x00 ? ((Position *) &(pMotor->sCurrent))->ustep : ~((Position *) &(pMotor->sCurrent))->ustep;
}

static void moveMotor(Motor *pMotor, int8_t state)
{
	// reverse direction if necessary
	if (pMotor->sTarget < pMotor->sCurrent)
	{
		pMotor->sCurrent = ~pMotor->sCurrent;
		pMotor->sTarget = ~pMotor->sTarget;
		pMotor->direction = ~pMotor->direction;
	}

	// trigger motor
	pMotor->enableMotor(-1);
	pMotor->state = state;
}

// movement routines
void initMotor(Motor *pMotor, OutMotor outMotor, EnableMotor enableMotor, uint8_t a, uint8_t referenceMask)
{
	pMotor->outMotor = outMotor;
	pMotor->enableMotor = enableMotor;
	setSemaphore(&(pMotor->done), 1);
	pMotor->a = a;
	pMotor->referenceMask = referenceMask;
	pMotor->state = Idle;
	pMotor->direction = 0x00;
	pMotor->sOffset = 0;
	pMotor->sCurrent = 0;
	pMotor->v = 0;
}

void referenceMotor(Motor *pMotor, int32_t s, uint16_t v, uint8_t switchPolarity)
{
	// ensure the motor is stopped
	resetMotor(pMotor, s, v, pMotor->referenceMask, switchPolarity&pMotor->referenceMask);

	// zero the motor position
	offsetMotor(pMotor);

	// move onto the switch
	moveMotor(pMotor, Slew);
}

void zeroMotor(Motor *pMotor)
{
	// zero the motor position
	offsetMotor(pMotor);
}

void slewMotor(Motor *pMotor, int32_t s, uint16_t v)
{
	// ensure the motor is stopped
	resetMotor(pMotor, s, v, 0x00, 0x00);

	// move to the position target
	moveMotor(pMotor, Slew);
}

#ifdef SCAN
void scanMotor(Motor *pMotor, int32_t s, uint16_t v)
{
	// ensure the motor is stopped
	resetMotor(pMotor, s, v, 0x00, 0x00);

	// scan at the target speed
	pMotor->t = 0;
	moveMotor(pMotor, pMotor->vMax <= pMotor->a ? SlowScan : FastScan);
}
#endif

void stopMotor(Motor *pMotor)
{
	// stop the motor
	pMotor->switchMask = 0;
	pMotor->switchPolarity = -1;
}

void waitMotor(Motor *pMotor)
{
	// wait for the motor to complete
	waitInterruptSemaphore(&(pMotor->done));

	// allow for next wait
	signalInterruptSemaphore(&(pMotor->done));
}

bool busyMotor(Motor *pMotor)
{
	int8_t state;
	cli();
	state = pMotor->state;
	sei();
	return state != Idle;
}
