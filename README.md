library for Atmel AVR processor with pre-emptive round-robin scheduler and task communication interface, motor drivers and pulse modulators, serial port driver, non-volatile memory driver, fixed point mathematics routines

Copyright 1998-2001 Glenn McIntosh

licensed under the GNU General Public License version 3, see [LICENSE.md](LICENSE.md)
