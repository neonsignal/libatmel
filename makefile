# GNU makefile
# GNU C to Atmel compiler

# definitions
MCU = atmega103
LIBRARY = libatmel
LIBRARY_PATH = e:/source/libatmel
TEST_SRC = libatmel.a makefile Doxyfile *.h *.c

# all
all: $(LIBRARY).rom $(LIBRARY).eep html/index.html html/libatmel.tar.gz
html/index.html: $(LIBRARY).elf
	c:/programs/doxygen/bin/doxygen doxyfile
html/libatmel.tar.gz: $(LIBRARY).elf
	tar -c $(TEST_SRC) >html/$(LIBRARY).tar
	gzip -f html/$(LIBRARY).tar
clean:
	rm -f *.o

# cc compilation
%.o : %.c 
	avr-gcc -c -g -O -Wall -Wstrict-prototypes -Wa,-ahlms=$(<:.c=.lst) -mmcu=$(MCU) -I. $< -o $@

# elf linking
%.elf: $(LIBRARY).o
	avr-gcc -o $@ $(LIBRARY).o -latmel -Wl,-Map=$(LIBRARY).map,--cref -L$(LIBRARY_PATH)

# avr rom creation
%.rom: %.elf
	avr-objcopy -O srec $< $@

%.eep: %.elf
	avr-objcopy -j .eeprom --set-section-flags=.eeprom="alloc,load" -O srec $< $@

# target dependencies
$(LIBRARY).o : monitor.h motor.h
$(LIBRARY).elf : libatmel.a

# buffer dependencies
buffer.o : buffer.h

# math dependencies
fix32.o : fix32.h

# monitor dependencies
monitor.o : buffer.h monitor.h machine.c
switch.o: monitor.h
time.o : monitor.h
semaph.o : monitor.h
mailbox.o : buffer.h monitor.h
state.o : monitor.h
trigger.o : monitor.h
comm.o : buffer.h monitor.h
nvmem.o : buffer.h monitor.h

# motor dependencies
motor.o : motor.h monitor.h
pulse.o : motor.h

# library dependencies
OBJ = buffer.o fix32.o monitor.o switch.o time.o semaph.o mailbox.o state.o trigger.o comm.o nvmem.o motor.o pulse.o
libatmel.a: $(OBJ)
	rm -f libatmel.a
	avr-ar r libatmel.a $(OBJ)
