// Copyright 1989-2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
/** @file buffer.h Circular buffer routines.
	*/
#ifndef _BUFFER_H
#define _BUFFER_H

// include files
#include <types.h>

// buffer
/** Circular buffer.
	These are not task safe. They are used by other monitor functions
	(mailboxes and communications).
	@see initBuffer, putBuffer, getBuffer, flushBuffer
	*/
typedef struct
{
	/** Length. */
	uint length;
	/** Buffer pointers. */
	uint head, tail;
	/** Buffer space. */
	uchar *buffer;
} Buffer;

// access routines
/** Initialize a buffer.
	This initializes to a specified size, usually called before the buffer is
	used. The buffer space is supplied by the calling function.

	@param pBuffer Pointer to a buffer structure.

	@param buffer Buffer space (byte array).

	@param buflen Length of the buffer (bytes).

	@see putBuffer
	*/
extern void initBuffer(Buffer *pBuffer, uchar *buffer, uint bufLen);

/** Put a message into a buffer.
	This sends in first in first out order. It does not check for overflow.

	@param pBuffer Pointer to a buffer structure.

	@param c Message (byte) to be placed in the buffer.

	@code
		Buffer foo;
		uchar buffer[100];
		initBuffer(&foo, buffer, sizeof(buffer));
		...
		uchar data;
		{generate data}
		putBuffer(&foo, data);
		...
		uchar data;
		data = getBuffer(&foo);
		{make use of data}
		...
		flushBuffer();
	@endcode
	*/
extern void putBuffer(Buffer *pBuffer, uchar c);

/** Get a message from a buffer.
	This receives in first in first out order. It does not check for underflow.

	@param pBuffer Pointer to a buffer structure.

	@return message (byte)

	@see putBuffer
	*/

extern uchar getBuffer(Buffer *pBuffer);
/** Clear a buffer.

	@param pBuffer Pointer to a buffer structure.

	@see putBuffer
	*/
extern void flushBuffer(Buffer *pBuffer);
#endif
