// Copyright 2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// Math
// fixed point integer arithmetic
// include files
#include <types.h>
#include "fix32.h"

// multiply
ufix32 mul32(ufix32 multiplier, ufix32 multiplicand)
{
	int32_t remainder = 0, product = multiplier;
	int8_t bits = 32;
	do
	{
		asm
		(
		    // shift left multiplier:product:remainder
		    "lsl %A1\n" "rol %B1\n" "rol %C1\n" "rol %D1\n" "rol %A0\n" "rol %B0\n" "rol %C0\n" "rol %D0\n"
		    // if multiplier set
		    "brcc noadd%=\n"
		    // add multiplicand
		    "add %A1,%A2\n" "adc %B1,%B2\n" "adc %C1,%C2\n" "adc %D1,%D2\n" "adc %A0,__zero_reg__\n" "adc %B0,__zero_reg__\n" "adc %C0,__zero_reg__\n" "adc %D0,__zero_reg__\n"
		    "noadd%=:"
		    : "+r"(product), "+r"(remainder) : "r"(multiplicand)
		);
	}
	while (--bits != 0);
	return product;
}

// divide
ufix32 div32(ufix32 dividend, ufix32 divisor)
{
	int32_t remainder = dividend, quotient = 0;
	int8_t bits = 32;
	do
	{
		asm
		(
		    // shift left remainder::dividend::quotient
		    "lsl %A0\n" "rol %B0\n" "rol %C0\n" "rol %D0\n" "rol %A1\n" "rol %B1\n" "rol %C1\n" "rol %D1\n"
		    "brcs sub%=\n"
		    // if remainder >= divisor
		    "cp %A1,%A2\n" "cpc %B1,%B2\n" "cpc %C1,%C2\n" "cpc %D1,%D2\n"
		    "brlo nosub%=\n"
		    "sub%=:"
		    // subtract divisor
		    "sub %A1,%A2\n" "sbc %B1,%B2\n" "sbc %C1,%C2\n" "sbc %D1,%D2\n"
		    // set quotient bit
		    "ori %A0,1\n"
		    "nosub%=:"
		    : "+r"(quotient), "+r"(remainder) : "r"(divisor)
		);
	}
	while (--bits != 0);
	return quotient;
}

// ratio
ufix32 ratio32(ufix32 value, ufix32 multiplier, ufix32 divisor)
{
	int32_t productLo = 0, productHi = multiplier;
	int8_t bits;
	bits = 32;
	do
	{
		asm
		(
		    // shift left multiplier:productHi:productLo
		    "lsl %A1\n" "rol %B1\n" "rol %C1\n" "rol %D1\n" "rol %A0\n" "rol %B0\n" "rol %C0\n" "rol %D0\n"
		    // if multiplier set
		    "brcc noadd%=\n"
		    // add value
		    "add %A1,%A2\n" "adc %B1,%B2\n" "adc %C1,%C2\n" "adc %D1,%D2\n" "adc %A0,__zero_reg__\n" "adc %B0,__zero_reg__\n" "adc %C0,__zero_reg__\n" "adc %D0,__zero_reg__\n"
		    "noadd%=:"
		    : "+r"(productHi), "+r"(productLo) : "r"(value)
		);
	}
	while (--bits != 0);
	bits = 32;
	do
	{
		asm
		(
		    // shift left productHi::productLo::ratio
		    "lsl %A0\n" "rol %B0\n" "rol %C0\n" "rol %D0\n" "rol %A1\n" "rol %B1\n" "rol %C1\n" "rol %D1\n"
		    "brcs sub%=\n"
		    // if productHi >= divisor
		    "cp %A1,%A2\n" "cpc %B1,%B2\n" "cpc %C1,%C2\n" "cpc %D1,%D2\n"
		    "brlo nosub%=\n"
		    "sub%=:"
		    // subtract divisor
		    "sub %A1,%A2\n" "sbc %B1,%B2\n" "sbc %C1,%C2\n" "sbc %D1,%D2\n"
		    // set productLo bit
		    "ori %A0,1\n"
		    "nosub%=:"
		    : "+r"(productLo), "+r"(productHi) : "r"(divisor)
		);
	}
	while (--bits != 0);
	return productLo;
}

// square root
ufix32 sqrt32(ufix32 dividend)
{
	int32_t remainder = 0, quotient = 0;
	int8_t bits = 32;
	do
	{
		asm
		(
		    // shift left remainder:dividend
		    "lsl %A2\n" "rol %B2\n" "rol %C2\n" "rol %D2\n" "rol %A1\n" "rol %B1\n" "rol %C1\n" "rol %D1\n"
		    // shift left quotient
		    "lsl %A0\n" "rol %B0\n" "rol %C0\n" "rol %D0\n"
		    // if remainder >= 2*quotient+1
		    "cpi %D2,0x80\n" "cpc %A1,%A0\n" "cpc %B1,%B0\n" "cpc %C1,%C0\n" "cpc %D1,%D0\n"
		    "brlo nosub%=\n"
		    // subtract 2*quotient+1
		    "subi %D2,0x80\n" "sbc %A1,%A0\n" "sbc %B1,%B0\n" "sbc %C1,%C0\n" "sbc %D1,%D0\n"
		    // set quotient bit
		    "ori %A0,1\n"
		    "nosub%=:"
		    // shift left remainder:dividend
		    "lsl %A2\n" "rol %B2\n" "rol %C2\n" "rol %D2\n" "rol %A1\n" "rol %B1\n" "rol %C1\n" "rol %D1\n"
		    : "+r"(quotient), "+r"(remainder) : "r"(dividend)
		);
	}
	while (--bits != 0);
	return quotient;
}
