// Copyright 2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// non-volatile memory access
// include files
#include <avr/interrupt.h>
#include <avr/signal.h>
#include <avr/eeprom.h>
#include <types.h>
#include <buffer.h>
#include "monitor.h"

// buffer
enum {BUFLEN=0x20};
static Buffer nvmemBuffer;
static uchar nvmemData[BUFLEN];
static Semaphore nvmemBufferSemaphore, nvmemWriteSemaphore;
static uchar *pNvmem;

// interrupt
SIGNAL(SIG_EEPROM)
{
	if (!checkInterruptSemaphore(&nvmemWriteSemaphore))
	{
		cbi(EECR, EERIE);
		return;
	}
	uchar address, value;
	address = getBuffer(&nvmemBuffer);
	value = getBuffer(&nvmemBuffer);
	eeprom_write_byte((uint8_t *)(uint) address, value);
}

// memory access
void initNvmem(uchar *pNvmem0, uchar nvmemSize)
{
	// initialize counters and queues
	initBuffer(&nvmemBuffer, nvmemData, sizeof(nvmemData));
	setSemaphore(&nvmemBufferSemaphore, 1);
	setSemaphore(&nvmemWriteSemaphore, 0);

	// load non volatile block
	pNvmem = pNvmem0;
	if (eeprom_read_byte(0) == *pNvmem)
		eeprom_read_block(pNvmem, 0, nvmemSize);
	else
	{
		uchar offset;
		for (offset = 0; offset < nvmemSize; ++offset)
		{
			while (!eeprom_is_ready());
			eeprom_write_byte((uint8_t *)(uint) offset, pNvmem[offset]);
		}
	}
}

void storeNvmem(void *pNvVariable, uchar nvVariableSize)
{
	uchar offset;
	uchar *pValue = pNvVariable;
	for (offset = 0; offset < nvVariableSize; ++offset)
	{
		waitSemaphore(&nvmemBufferSemaphore);
		putBuffer(&nvmemBuffer, (pValue-pNvmem)+offset);
		putBuffer(&nvmemBuffer, pValue[offset]);
		signalSemaphore(&nvmemBufferSemaphore);
		cli();
		setInterruptSemaphore(&nvmemWriteSemaphore);
		sbi(EECR, EERIE);
		sei();
	}
}
