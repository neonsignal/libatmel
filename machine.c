// Copyright 1989-2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// Processor
// Atmel processor, GNU compiler

// include files
#include <avr/interrupt.h>

// base address of main stack
#define DEFAULT_STACK_TOP (RAMEND)

// processor 'CALL's post-decrement stack pointer
#define POSTDECREMENT_SP

// processor 'CALL's store address bytes in reverse on the stack
#define REVERSE_CALL_ADDRESS

// registers preserved around function calls
typedef struct // preservedRegisters
{
	uchar r2, r3, r4, r5, r6, r7, r8, r9, r10, r11, r12, r13, r14, r15, r16, r17, r28, r29;
} PreservedRegisters;

// monitor locking functions
static void inline lock(void)
{
	cli();
	switchCnt -= 0x7F;
	sei();
}
static bool inline unlock(void)
{
	uchar cnt;
	cli();
	cnt = (switchCnt += 0x7F);
	sei();
	return cnt < 0x80;
}

// monitor switching
// (changing these functions may affect register allocation)
// (GNU avr-gcc must have optimization at least level 1 to prevent frame pointer)
void switchMonitor(void)
{
	// save registers
	asm volatile ("push r28");
	asm volatile ("push r29");
	asm volatile ("push r16");
	asm volatile ("push r17");
	asm volatile ("push r14");
	asm volatile ("push r15");
	asm volatile ("push r12");
	asm volatile ("push r13");
	asm volatile ("push r10");
	asm volatile ("push r11");
	asm volatile ("push r8");
	asm volatile ("push r9");
	asm volatile ("push r6");
	asm volatile ("push r7");
	asm volatile ("push r4");
	asm volatile ("push r5");
	asm volatile ("push r2");
	asm volatile ("push r3");

	// check for stack overflow
#ifdef RUNTIME_DEBUG
	if (switchCnt < activeContext->freeTime)
		activeContext->freeTime = switchCnt;
#endif
#ifdef STACK_DEBUG
	if (activeContext->stackGuardbits != 0xA5)
		taskStackOverflow();
#endif

	// switch to next task
	{
		Context *context; // optimization hint
		activeContext->sp = (uchar *) inw(SPL);
		activeContext = context = activeContext->nextContext;
		cli();
		outw(SPL, (uint) context->sp);
		sei();
		switchCnt = context->runTime;
	}

	// restore registers
	asm volatile ("pop r3");
	asm volatile ("pop r2");
	asm volatile ("pop r5");
	asm volatile ("pop r4");
	asm volatile ("pop r7");
	asm volatile ("pop r6");
	asm volatile ("pop r9");
	asm volatile ("pop r8");
	asm volatile ("pop r11");
	asm volatile ("pop r10");
	asm volatile ("pop r13");
	asm volatile ("pop r12");
	asm volatile ("pop r15");
	asm volatile ("pop r14");
	asm volatile ("pop r17");
	asm volatile ("pop r16");
	asm volatile ("pop r29");
	asm volatile ("pop r28");
}

// timer interrupts
static uchar tickHz10 = 1, tickCnt = 1;
void timerFreq(uchar hz10)
{
	tickCnt = tickHz10 = hz10;
}
void timerTick(void)
{
	// assumes that interrupts are off,
	// may not return immediately
	// decrement clock counter
	if (--tickCnt == 0)
	{
		tickCnt = tickHz10;
		++clockCnt;
	}

	// timesharing switch
	if (switchCnt > 0)
		--switchCnt;
	while (switchCnt == 0x7F)
	{
		// disable switching
		switchCnt -= 0x7F; // lock()

		// switch monitor
		sei();
		switchMonitor();
		cli();

		// enable switching
		switchCnt += 0x7F; // unlock()
	}
}
uint getTime(void)
{
	uint clock;

	// get clock value
	cli();
	clock = clockCnt;
	sei();
	return clock;
}
