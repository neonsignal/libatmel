// Copyright 1989-2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// test program for monitor and motor library
// include files
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/signal.h>
#include <types.h>
#include <monitor.h>
#include <motor.h>
#include <fix32.h>

// motors
Motor motor0;
/*
static uint8_t fsTable[] = {0x00, 0x10, 0x90, 0x80};
static bool outMotorFs(uint8_t angle)
{
	uint8_t bits = fsTable[angle>>6];
	outb(PORTB, (inp(PORTB)&0x6F) | bits);
	return false;
}
*/
static uint8_t sineTablePwm[] = SineTable128;
static uint8_t outMotorPwm(uint8_t angle)
{
	outb(OCR0, sineTablePwm[angle]);
	outb(OCR2, sineTablePwm[(angle+64)&0xFF]);
	return 0;
}

static void enableMotor(uint8_t enable)
{
}
// pulse density modulators
Pulse dac0, dac1;

// led tasks
void invert(uchar led)
{
	cli();
	outb(PORTB, inp(PORTB) ^ led);
	sei();
}

uchar motorTaskStack[200];
void motorTask(void)
{
	for (;;)
	{
		slewMotor(&motor0, 25000, 2048);
		waitMotor(&motor0);
		invert(0x04);
		slewMotor(&motor0, 0, 2048);
		waitMotor(&motor0);
		invert(0x04);
	}
}

// timer initialization
#define T8
void initTimer(void)
{
	timerFreq(6000000/(8*510)/10); // Hz/10
#ifdef T8
	outb(TCCR0, 1<<PWM0|2<<COM00|1<<CS00);
	outb(TCCR2, 1<<PWM2|2<<COM20|1<<CS20);
#else
	outb(TCCR0, 1<<PWM0|2<<COM00|2<<CS00);
	outb(TCCR2, 1<<PWM2|2<<COM20|2<<CS20);
#endif
	outb(OCR0, 0x80);
	outb(OCR2, 0x80);
	sbi(TIMSK, TOIE0);
}
void deinitTimer(void)
{
	cbi(TIMSK, TOIE0);
}
SIGNAL(SIG_OVERFLOW0)
{
#ifdef T8
	static uint8_t cnt = 0;
#endif
	updatePdm(&dac0);
	updatePdm(&dac1);
#ifdef T8
	// divide by 8
	if ((cnt -= 256/8) == 0)
#endif
	{
		updateMotor(&motor0);
		timerTick(); // must be last
	}
}

// adc initialization
static uint reading = 0;
SIGNAL(SIG_ADC)
{
	static uchar index = 0;
	static uint buffer[4];
	buffer[index] = inw(ADCL);
	index = (index+1)&0x3;
	outb(PORTB, (inp(PORTB)&0xF8)|(index>>1));
	outb(ADMUX, ((index&0x1)+1)<<MUX0);
	sbi(ADCSR, ADSC);
	if (index == 0)
		reading += (buffer[0]-buffer[1]+buffer[3]-buffer[2]+0x20) - (reading>>6);
}

int main( void )
{
	// set up Port B as output
	outb(PORTB, ~0x00);
	outb(DDRB, 0xFF);

	// set up A to D converter
	outb(ADCSR, 1<<ADEN | 1<<ADSC | 1<<ADIE | 7<<ADPS0);

	// initialize monitor
	initMonitor(256, 2);

	// initialize motors
	initMotor(&motor0, outMotorPwm, enableMotor, 255, 0x00);

	// initialize pulse density modulators
	initPulse(&dac0, PORTB, 0x20);

	// initialize serial port
	initSerial();

	// start switching
	initTimer();

	// start monitor test tasks
	startTask(motorTask, motorTaskStack, sizeof(motorTaskStack), 0, 2);

	// main task test
	for (;;)
	{
		// pulse density modulators
		int adjust = 0;
		uint flow;
		static uint flow0 = 0;
		static unsigned int drive = 20000u;
		delayTime(1);

		// measure flow
		flow = reading;
		sendSerial(flow>>8);
		sendSerial(flow&0xFF);
		adjust = (int) (0x780-flow) >> 1; // P term
		adjust += (int) (flow0-flow) << 0; // D term
		flow0 = flow;

		// limited proportional feedback
		if (adjust < -2000) adjust = -2000;
		if (adjust > 2000) adjust = 2000;
		drive += adjust;

		// limit valve position
		if (drive > 45000u) drive = 45000u;
		if (drive < 30000u) drive = 30000u;

		// update output
		sendSerial(drive>>8);
		sendSerial(drive&0xFF);
		setPulse(&dac0, drive);

		// status LED
		invert(0x08);
	}
	return 0;
}
