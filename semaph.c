// Copyright 1989-2000 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
#include <types.h>
#include <avr/interrupt.h>
#include "monitor.h"

// semaphores
void setSemaphore(Semaphore *pSemaphore, uint cnt)
{
	// initialize semaphore
	stopSwitch();
	pSemaphore->cnt = cnt;
	startSwitch();
}

void signalSemaphore(Semaphore *pSemaphore)
{
	// signal semaphore
	stopSwitch();
	++pSemaphore->cnt;
	startSwitch();
}

void waitSemaphore(Semaphore *pSemaphore)
{
	// wait for semaphore
	stopSwitch();
	while (pSemaphore->cnt == 0)
		switchMonitor();
	--pSemaphore->cnt;
	startSwitch();
}

void waitTSemaphore(Semaphore *pSemaphore, uint time)
{
	// wait for semaphore or timeout
	stopSwitch();
	time += getTime();
	while (pSemaphore->cnt == 0 && (int) (time-getTime()) > 0)
		switchMonitor();
	if (pSemaphore->cnt == 0)
		activeContext->timeout = true;
	else
		--pSemaphore->cnt;
	startSwitch();
}

void setInterruptSemaphore(Semaphore *pSemaphore)
{
	// signal semaphore inside interrupt
	++pSemaphore->cnt;
}

bool checkInterruptSemaphore(Semaphore *pSemaphore)
{
	// check for semaphore inside interrupt
	if (pSemaphore->cnt == 0)
		return false;
	--pSemaphore->cnt;
	return true;
}

void signalInterruptSemaphore(Semaphore *pSemaphore)
{
	// signal semaphore to interrupt
	cli();
	++pSemaphore->cnt;
	sei();
}

void waitInterruptSemaphore(Semaphore *pSemaphore)
{
	// wait for semaphore from interrupt
	stopSwitch();
	cli();
	while (pSemaphore->cnt == 0)
	{
		sei();
		switchMonitor();
		cli();
	}
	--pSemaphore->cnt;
	sei();
	startSwitch();
}

void waitTInterruptSemaphore(Semaphore *pSemaphore, uint time)
{
	// wait for semaphore from interrupt or timeout
	stopSwitch();
	time += getTime();
	cli();
	while (pSemaphore->cnt == 0 && (int) (time-getTime()) > 0)
	{
		sei();
		switchMonitor();
		cli();
	}
	if (pSemaphore->cnt == 0)
		activeContext->timeout = true;
	else
		--pSemaphore->cnt;
	sei();
	startSwitch();
}
