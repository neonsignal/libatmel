// Copyright 1989-2000 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// Comm
// include files
#include <types.h>
#include <avr/io.h>
#include <avr/signal.h>
#include <avr/interrupt.h>
#include <buffer.h>
#include "monitor.h"

// serial port buffers
enum {SBUFLEN=0x40};
static Buffer serialInBuffer, serialOutBuffer;
static uchar serialInData[SBUFLEN], serialOutData[SBUFLEN];
static Semaphore serialInSemaphore, serialOutSemaphore;

// communications
SIGNAL(SIG_UART_DATA)
{
	// wait for character
	if (!checkInterruptSemaphore(&serialOutSemaphore))
	{
		cbi(UCR, UDRIE);
		return;
	}

	// send character
	outb(UDR, getBuffer(&serialOutBuffer));
}

SIGNAL(SIG_UART_RECV)
{
	// receive character
	putBuffer(&serialInBuffer, inp(UDR));

	// signal character available
	setInterruptSemaphore(&serialInSemaphore);
}

void initSerial()
{
	// initialize counters and mailboxes
	initBuffer(&serialInBuffer, serialInData, sizeof(serialInData));
	setSemaphore(&serialInSemaphore, 0);
	initBuffer(&serialOutBuffer, serialOutData, sizeof(serialOutData));
	setSemaphore(&serialOutSemaphore, 0);

	// set mode and clear pending data
	outb(UBRR, 6000000/(9600*16l)-1);
	inp(UDR);

	// enable interrupts
	outb(UCR, (1<<RXCIE)|(1<<TXEN)|(1<<RXEN));
}

void sendSerial(uchar message)
{
	// send character
	putBuffer(&serialOutBuffer, message);

	// signal character available
	cli();
	setInterruptSemaphore(&serialOutSemaphore);
	sbi(UCR, UDRIE);
	sei();
}

uchar receiveSerial(void)
{
	// wait for character
	waitInterruptSemaphore(&serialInSemaphore);

	// receive character
	return getBuffer(&serialInBuffer);
}

uchar receiveTSerial(uint time)
{
	// wait for character
	waitTInterruptSemaphore(&serialInSemaphore, time);

	// receive character
	if (activeContext->timeout)
		return 0;
	return getBuffer(&serialInBuffer);
}
