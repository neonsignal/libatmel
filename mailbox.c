// Copyright 1989-2000 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// Mailbox
// include files
#include <types.h>
#include <buffer.h>
#include "monitor.h"

// mailboxes
void initMailbox(Mailbox *pMailbox, uchar *buffer, uint bufLen)
{
	// initialize
	initBuffer(&(pMailbox->buffer), buffer, bufLen);
	setSemaphore(&(pMailbox->semaphore), 0);
}

void sendMailbox(Mailbox *pMailbox, uchar message)
{
	// put message in buffer
	putBuffer(&(pMailbox->buffer), message);

	// signal message in buffer
	signalSemaphore(&(pMailbox->semaphore));
}

uchar receiveMailbox(Mailbox *pMailbox)
{
	// wait for message in buffer
	waitSemaphore(&(pMailbox->semaphore));

	// get message from buffer
	return getBuffer(&(pMailbox->buffer));
}

uchar receiveTMailbox(Mailbox *pMailbox, uint time)
{
	// wait for message in buffer or timeout
	waitTSemaphore(&(pMailbox->semaphore), time);

	// get message from buffer
	if (!activeContext->timeout)
		return getBuffer(&(pMailbox->buffer));
	return 0;
}
