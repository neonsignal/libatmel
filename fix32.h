// Copyright 2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
/** @file fix32.h Fixed point integer arithmetic library.
	A program that requires use of the maths library will link
	libatmel.a after any object files. Any file that uses library
	functions will include fix32.h.

	All routines take 32 bit operands and produce 32 bit results. By
	default, these unsigned values have type 'ufix32', a format of 0.32 bits.

	Calculations use exact methods (rather than iteration or other
	approximations), but results are truncated rather than rounded. They
	assume a processor without a multiplier.

	Calculation times depend on the complexity of the operation.
	Multiplication and division are unit operations (eg taking about 90 us
	on a 6MHz Atmel processor). Ratios and square roots take the time of two
	unit operations.

	Much of the coding is in assembler, so rewriting for another processor
	would require a significant amount of time. The implementation assumes
	that there are enough registers for the operation (typically around 13 x
	8 bit registers).

	Glossary of terms:
	@li
		@b fixed point number: a representation of a number using an integer
		value with an implicit decimal point (actually a binary representation
		in this library)
	@li
		@b format: the binary representation of the fixed point number; this
		document uses the notation [Bx], where x specifies the number of bits to
		the left of the decimal point that have been represented (this can be
		negative)
	@li
		@b unit @b operation: the fastest arithmetic operations (such as
		multiplication) that can be calculated with only one pass on each bit
	*/

#ifndef _IMATH_H
#define _IMATH_H
// include files
#include <types.h>

// definitions
/** Fixed point integer (32 bits).
	The ufix32 declaration is used to define the operands and results of all
	the routines in the library. It can be used interchangably with any 32
	bit unsigned integer declaration.

	<CODE>
		ufix32 value0 = UFIX32(0.45), value1 = UFIX32(0.74), value2;<BR>
	</CODE>
	*/
typedef uint32_t ufix32;

/** Convert floating point to fixed point.
	This macro takes a constant >= 0.0 and < 1.0, and converts it into a
	ufix32.
	*/
#define UFIX32(x) ((ufix32) ((x)*4294967296))

// mathematical routines
/** Multiply two numbers.
	All input values are acceptable: the function cannot overflow. The
	precision of the calculation is multiplier [Bx] * multiplicand [By] =
	product [Bx+y].

	@return Product (fixed point number).

	@param multiplier Multiplier (fixed point number)

	@param multiplicand Multiplicand (fixed point number).

	<CODE>
		ufix32 multiplier = UFIX32(0.45), multiplicand = UFIX32(0.74);
		ufix32 product;
		...
		product = mul32(multiplier, multiplicand);
	</CODE>
	*/
ufix32 mul32(ufix32 multiplier, ufix32 multiplicand);

/** Divide two numbers.
	If the divisor is less than or equal to the dividend, the function will
	overflow with a meaningless result. The precision of the calculation is
	dividend [Bx] / divisor [By] = quotient [Bx-y].

	@return Quotient (fixed point number).

	@param dividend Dividend (fixed point number).

	@param divisor Divisor (fixed point number).

	<CODE>
		ufix32 dividend = UFIX32(0.45), divisor = UFIX32(0.74);
		ufix32 quotient;
		...
		quotient = div32(dividend, divisor);
	</CODE>
	*/
ufix32 div32(ufix32 dividend, ufix32 divisor);

/** Multiply a number by a ratio.
	If the result is greater than or equal to 1/ratio, the function will
	overflow with a meaningless result. The precision of the calculation is
	value [Bx] * (multiplier [By] / divisor [Bz]) = result [Bx+(y-z)].

	Because the intermediate calculation uses 64 bit precision, it does not
	matter if the multiplier and divider are relatively small integers.

	@return Result (fixed point number).

	@param value Value (fixed point number).

	@param multiplier Multiplier (fixed point number or unsigned integer).

	@param divisor Divisor (fixed point number or unsigned integer).

	<CODE>
		ufix32 value = UFIX32(0.45);
		ufix32 result;
		...
		result = ratio32(value, 4, 3);
	</CODE>
	*/
ufix32 ratio32(ufix32 value, ufix32 multiplier, ufix32 divisor);

/** Square root of a number.
	All input values are acceptable: the function cannot overflow. The
	precision of the calculation is sqrt(dividend [Bx]) = quotient [Bx/2].

	@return Quotient (fixed point number).

	@param dividend Dividend (fixed point number).

	<CODE>
		ufix32 dividend = UFIX32(0.45);
		ufix32 quotient;
		...
		quotient = sqrt32(dividend);
	</CODE>
	*/
ufix32 sqrt32(ufix32 dividend);
#endif
