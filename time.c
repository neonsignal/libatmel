// Copyright 1989-2000 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// Time
// include files
#include <types.h>
#include <avr/interrupt.h>
#include "monitor.h"

// time delays
void delayTime(uint time)
{
	// wait for timer
	stopSwitch();
	time += getTime();
	while ((int) (time - getTime()) > 0)
		switchMonitor();
	startSwitch();
}
