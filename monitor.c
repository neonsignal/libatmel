// Copyright 1989-2000 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
#include <types.h>
#include "monitor.h"

// monitor state
Context *activeContext;
static uint clockCnt = 0;
static volatile uchar switchCnt = 1;

// machine dependent definitions and functions
#include "machine.c"

// monitor switch locking functions
void startSwitch(void)
{
	// enable switching
	while (unlock())
		switchMonitor();
}
void stopSwitch(void)
{
	// disable switching
	lock();
}

// monitor functions
// initialization
void initMonitor(uint stackSize, uchar runTime)
{
	// setup task queue
	activeContext = (Context *) (DEFAULT_STACK_TOP-stackSize);
	activeContext->nextContext = activeContext;
	activeContext->prevContext = activeContext;
	activeContext->runTime = runTime;
#	ifdef RUNTIME_DEBUG
	activeContext->freeTime = 0;
#	endif
	activeContext->timeout = false;
#	ifdef STACK_DEBUG
	activeContext->stackGuardbits = 0xA5;
#	endif

	// start switch
	startSwitch();
}

// task entry/exit
typedef struct // stackFrame
{
	PreservedRegisters preservedRegisters;
	void (*retFn)(void), (*fn)(void);
} StackFrame;
void startTask(void (*fn)(void), void *stack, uint stackSize, void *user, uchar runTime)
{
	StackFrame *stackFrame = stack + stackSize - sizeof(StackFrame);
	Context *context = stack;

	// setup stack
#	ifndef REVERSE_CALL_ADDRESS
	stackFrame->fn = fn;
	stackFrame->retFn = startSwitch;
#	else
	typedef union
	{
		struct
		{
			uint8_t lo, hi;
		} byte;
		uint16_t word;
	} byte_word;
	* (uchar *) &(stackFrame->fn) = ((byte_word) (uint) fn).byte.hi;
	* ((uchar *) &(stackFrame->fn)+1) = ((byte_word) (uint) fn).byte.lo;
	* (uchar *) &(stackFrame->retFn) = ((byte_word) (uint) startSwitch).byte.hi;
	* ((uchar *) &(stackFrame->retFn)+1) = ((byte_word) (uint) startSwitch).byte.lo;
#	endif

	// insert into task queue
#	ifndef POSTDECREMENT_SP
	context->sp = (uchar *) stackFrame;
#	else
	context->sp = (uchar *) stackFrame-1;
#	endif
	context->runTime = runTime;
#	ifdef RUNTIME_DEBUG
	context->freeTime = 0;
#	endif
	context->user = user;
#	ifdef STACK_DEBUG
	context->stackGuardbits = 0xA5;
#	endif
	stopSwitch();
	context->nextContext = activeContext->nextContext;
	context->prevContext = activeContext;
	context->nextContext->prevContext = context;
	activeContext->nextContext = context;
	startSwitch();
}

void exitTask(Semaphore *pSemaphore)
{
	// exit to next task
	stopSwitch();
	if (pSemaphore)
		++pSemaphore->cnt;
	activeContext->prevContext->nextContext = activeContext->nextContext;
	activeContext->nextContext->prevContext = activeContext->prevContext;
	switchMonitor();
	for (;;); // halt (reaches this point only if all tasks exit)
}

// timeout access
bool resetTimeout(void)
{
	bool timeout;
	timeout = activeContext->timeout;
	activeContext->timeout = false;
	return timeout;
}
