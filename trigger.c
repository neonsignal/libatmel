// Copyright 1989-2000 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// Trigger
// include files
#include <types.h>
#include "monitor.h"

// triggers
void initTrigger(Trigger *pTrigger)
{
	// set trigger
	stopSwitch();
	pTrigger->bits = 0;
	startSwitch();
}

void changeTrigger(Trigger *pTrigger, uint8_t bits)
{
	// change trigger
	stopSwitch();
	pTrigger->bits ^= bits;
	startSwitch();
}

uint8_t waitTrigger(Trigger *pTrigger, uint8_t bits)
{
	uint8_t bits0, bits1;
	// wait for trigger
	stopSwitch();
	bits0 = pTrigger->bits & bits;
	while ((bits1 = (pTrigger->bits & bits) ^ bits0) == 0)
		switchMonitor();
	startSwitch();
	return bits1;
}

uint8_t waitTTrigger(Trigger *pTrigger, uint8_t bits, uint time)
{
	uint8_t bits0, bits1;
	// wait for trigger or timeout
	stopSwitch();
	time += getTime();
	bits0 = pTrigger->bits & bits;
	while ((bits1 = (pTrigger->bits & bits) ^ bits0) == 0 && (int) (time-getTime()) > 0)
		switchMonitor();
	if (bits1 == 0)
		activeContext->timeout = true;
	startSwitch();
	return bits1;
}
