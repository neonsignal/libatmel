// Copyright 2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
/** @file motor.h Motor drivers and pulse modulators library.
	A program that requires use of the motor library will link libatmel.a after
	any object files. Any file that uses library functions will include motor.h.

	Motors are interrupt driven, with all movement calculations being done
	inside the interrupt function. This calculation takes a reasonably
	consistent time period, whether the motor is ramping or moving steadily (for
	example, 30 to 45 us on a 6MHz Atmel processor), and about half that time
	when the motor is idle. Calculations involve only integer additions and
	subtractions (so that this library can be used on processors with
	non-existent or slow multiply instructions).

	Functions to initiate movement do not wait (unless the motor is already
	moving, in which case they stop the current movement and suspend until the
	motor has come to a halt before initiating the new movement). This means
	that in many circumstances these functions do not need to be run in separate
	tasks.

	Motor acceleration can vary from 1 to 255 nanosteps/tick<SUP>2</SUP> (for
	example, from about 1/15 of a second up to 16 seconds acceleration time for
	a 1 ms tick and a maximum step rate of 1 kHz). Although the software allows
	a motor velocity value up to 65535, the effective range is from 1 to 16384
	nanosteps/tick (for example, from about 0.5 revolutions/hour up to 2.5
	revolutions/second for a 1ms tick and a 0.9 degree motor). Positions range
	from -214718111 to 2147418111 nanosteps (for example, a range of about 650
	revolutions for a 0.9 degree motor).

	Motor structures and functions are not task-safe. If two tasks need to
	access the same motor, they would use an external semaphore to control
	access to the motor as a resource. The motor library uses the task library,
	but only to communicate between the interrupt driven update function and the
	movement control functions to indicate the completion of a movement.

	Pulse density modulators are interrupt driven, with the modulation function
	being calculated inside the interrupt. This calculation takes a consistent
	time period regardless of the output value (for example, 9 us on a 6MHz
	Atmel processor).

	Modulator average values can vary from 0 to 65535 (that is, from 0.000% to
	99.998%). Though the resolution is 16 bits, the effective resolution is
	governed by the ratio between the modulation frequency and the hardware
	filtering of the output (for example, a resolution of about 8 bits for a 10
	kHz modulation frequency and a 40 Hz low-pass filter).

	The output of the pulse modulators is inverted (active low).

	Glossary of Terms:
	@li
		@b acceleration: angular acceleration of the motor, measured in
		nanosteps/tick<SUP>2</SUP> (unsigned 8 bits)
	@li
		@b microstep: 64th part of a step (linearity will depend on the
		hardware)
	@li
		@b modulation @b frequency: the pulse width for a pulse modulated output
	@li
		@b nanostep: 256th part of a microstep (these are not mapped to the
		output; instead they enable granularity for acceleration, velocity and
		position parameters so that there are no roundoff errors)
	@li
		@b position: angular position of the motor, measured in nanosteps
		(signed 32 bits)
	@li
		@b pulse @b density @b modulation: a stream of fixed width pulses in which
		the average is set by changing the pulse density; the pulses are evenly
		distributed
	@li
		@b pulse @b width @b modulation: a stream of variable width pulses in
		which the average is set by changing the pulse width; the pulses are at
		fixed intervals
	@li
		@b quadstep: four steps (in a motor sequence, the phase pattern
		repeats after each quadstep)
	@li
		@b ramping: acceleration and deacceleration of a motor
	@li
		@b reference: turn a motor until it reaches a hardware defined
		position (such as a fixed switch)
	@li
		@b scan: turn a motor between two positions at a nominated constant
		velocity
	@li
		@b slew: turn a motor to a nominated position as fast as possible
	@li
		@b step: the angular distance between the poles on a stepper motor
		code
	@li
		@b tick: the constant time between updates of the motor position
	@li
		@b velocity: angular speed of the motor, measured in
		nanosteps/tick (unsigned 16 bits)
	*/
#ifndef _MOTOR_H
#define _MOTOR_H
// include files
#include <types.h>
#include <monitor.h>

// defines
/** Compile scan routines.
	This define can be commented out and the library recompiled if scan
	movement is not needed.
	*/
#define SCAN

/** Little endian processor.
	This define indicates that words are stored low byte first. The library
	must be recompiled if this is commented out to indicate high byte first.
	*/
#define LITTLE_ENDIAN

// motor
/** Set motor position.
	This is a user function supplied to initMotor(). It is called from an
	interrupt routine, so the interrupt enable flag should not be changed.

	@return Reference switch state (bit pattern).
	This is checked by the referenceMotor() function.

	@param angle Motor angle (microsteps).
	The step position is an 8 bit 'angle' that is the relative microstep
	position within a range of 0 to 1 quadsteps. This must be converted to a
	phase pattern or microstep value. Predefined sine tables can be used for
	this conversion. The tables contain 256 entries (a complete sine-wave) of
	unsigned 8 bit values, with a mid-point value of 127.5 (the same scheme as
	used by the pulse width modulators on the Atmel processors). SineTable256
	provides the full 0 to 255 range of values, while the other tables
	(SineTable128, SineTable64, SineTable32) are reduced for motors with
	voltage/current capabilities less than the supply.

	@code
		static uint8_t fullstepTable[] = {0x30, 0x60, 0xC0, 0x90};
		static uint8_t outMotorFullstep(uint8_t angle)
		{
			uint8_t bits = fullstepTable[angle>>6];
			~output bits to a port~
			~return state of reference switches~
		}
		...
		static uint8_t sineTablePwm[] = SineTable128;
		static uint8_t outMotorPwm(uint8_t angle)
		{
			outb(OCR0, sineTablePwm[angle]);
			outb(OCR2, sineTablePwm[(angle+64)&0xFF]);
			~return state of reference switches~
		}
	@endcode
	*/
typedef uint8_t (*OutMotor)(uint8_t angle);

/** Enable power to motor.
	This is a user function supplied to initMotor(). It is called from an
	interrupt routine, so the interrupt enable flag should not be changed.

	@param enable Enable power to motor (8 bit flag, 0=disable, -1=enable).
	Not all motors need to be disabled (that is, this function can have an
	empty body).

	@code
		static void enableMotor(uint8_t enable)
		{
			send enable bit to a port
		}
	@endcode
	*/
typedef void (*EnableMotor)(uint8_t enable);

/** Motor.
	Each motor requires the declaration of a structure that holds state
	information. Members of the motor structure are not intended to be
	accessed directly by the user. The structure uses about 25 bytes, and has
	no buffers.

	Fixed parameters within the structure are set by initMotor(). Parameters
	that control a particular movement are set by the movement functions
	(referenceMotor(), zeroMotor(), and slewMotor()). The structure is used by
	the interrupt function updateMotor() to calculate the motor movements.

	@code
		Motor motor0, motor1, motor2;
	@endcode
	*/
typedef struct
{
	// motor parameters
	/** Motor output routine. */
	OutMotor outMotor;
	/** Motor enable routine. */
	EnableMotor enableMotor;
	/** Acceleration (nanosteps/tick/tick). */
	uint8_t a;
	// movement parameters
	/** Current state. */
	int8_t state;
	/** Movement done. */
	Semaphore done;
	/** Target direction (0x00=forward, 0xFF=reverse). */
	uint8_t direction;
	/** Offset to position (nanosteps). */
	int16_t sOffset;
	/** Current position (nanosteps). */
	int32_t sCurrent;
	/** Target position (nanosteps). */
	int32_t sTarget;
	/** Current velocity (nanosteps/tick). */
	uint16_t v;
	/** Target velocity (nanosteps/tick). */
	uint16_t vMax;
#ifdef SCAN
	/** Deacceleration time for prescan (ticks). */
	uint16_t t;
	// switch parameters
	/** Reference switch mask (bits). */
	uint8_t referenceMask;
	/** Current switch mask (0=ignore switch or abort, referenceMask=watch reference switch). */
	uint8_t switchMask;
	/** Reference switch polarity (0xFF=abort, bits=switch state). */
	uint8_t switchPolarity;
#endif
} Motor;

// movement routines
/** Initialize the motor.
	This sets the fixed parameters and clears the motor position. It must be
	called before the interrupt that calls updateMotor() is enabled.

	@param pMotor Pointer to a motor structure.

	@param outMotor Motor output function.
	This is a user-supplied function that copies an angular step position to
	the hardware and returns the reference switch state. The interrupt enable
	state on entry will be the same as on entry to updateMotor();

	@param enableMotor Enable the motor.
	This is a user-supplied function to control motor power.

	@param a Angular acceleration of the motor (nanosteps/tick<SUP>2</SUP>, 1 to 255).
	It is usually constrained by the inertia of the system and the torque of
	the motor (assuming negligible friction, a constant-current motor phase
	supply will achieve a relatively constant torque, while a fixed voltage
	motor phase supply will result in a torque that reduces with velocity).
	There is no provision for a variable acceleration, so the acceleration
	must be reduced to the worst case (typically the maximum velocity).

	@param referenceMask Reference switch mask (bit pattern).
	This is used to mask out irrelevant bits from the reference switch state
	returned by updateMotor();

	@code
		Motor motor;
		...
		initMotor(&motor, outMotorFullstep, enableMotor, 10, 0x20);
	@endcode
	*/
extern void initMotor(Motor *pMotor, OutMotor outMotor, EnableMotor enableMotor, uint8_t a, uint8_t referenceMask);

/** Update the motor position.
	This is called by a user supplied interrupt routine. Each motor requires a
	separate call to the function.

	Inside the function, the motor position is updated by calling
	Motor::outMotor() before the next position is calculated.

	Because updateMotor() calculations are not reentrant, it is important to
	make sure that it is not suspended in a multi-tasking environment.
	However, the function itself makes no assumptions about the interrupt
	enable state.

	@param pMotor is a pointer to a motor structure.

	@code
		Motor motorA, motorB;
		...
		void __attribute__((signal)) motorInterrupt(void)
		{
			updateMotor(&motorA);
			updateMotor(&motorB);
			...
		}
		...
		initMotor(&motorA, outMotorPwm, 10);
		initMotor(&motorB, outMotorFullstep, 20);
		...
		~enable motor interrupt~
	@endcode
	*/
extern void updateMotor(Motor *pMotor);

/** Slew a motor toward the reference position.
	This turns the motor while checking for a reference condition to become
	true. When the reference point or the maximum distance is reached, the
	motor is brought to a stop. The function will abort any current movement,
	but will not wait for the reference movement to complete before returning.

	@param pMotor Pointer to a motor structure.

	@param s Maximum relative position (nanosteps, -214718111 to 2147418111).
	This position limits the movement in relation to the current position
	(which is reset to 0 at the start of the move). If the reference point is
	not found, the motor will stop at this limit.

	@param v Maximum slew speed (nanosteps/tick, 1 to 65535).
	It should not be made too high, or the overrun distance required to
	deaccelerate the motor may cause mechanical problems. Typically the motor
	would be referenced then dereferenced, the first time checking for closing
	of the reference switch at a fast speed to find the rough position, the
	second time checking for opening of the reference switch at a slow speed
	(less than the acceleration) in the reverse direction to find the exact
	reference position.

	@param switchPolarity Reference switch polarity (0=off, -1=on, bits=matching state)
	This pattern is the reference switch state that will cause the movement to
	conclude. Bits that are not in the referenceMask given to initMotor() will
	be ignored.

	@code
		referenceMotor(&motor, -500*65536l, 1000, 0);
		waitMotor(&motor);
		referenceMotor(&motor, 100*65535l, 10, -1);
		waitMotor(&motor);
		zeroMotor(&motor);
	@endcode
	*/
extern void referenceMotor(Motor *pMotor, int32_t s, uint16_t v, uint8_t switchPolarity);

/** Set a motor position to zero.
	This resets the current position. It would typically be called after the
	motor is referenced.

	@param pMotor Pointer to a motor structure.

	@see referenceMotor
	*/
extern void zeroMotor(Motor *pMotor);

/** Slew a motor to a point.
	The motor is deaccelerated so that it will stop at the desired point. It
	is designed to never overrun the position (which might cause backlash
	issues). The function will abort any current movement, but will not wait
	for the slew movement to complete before returning.

	@param pMotor Pointer to a motor structure.

	@param s Ttarget position (nanosteps, -214718111 to 2147418111).

	@param v Maximum slew speed (nanosteps/tick, 1 to 65535).
	It is constrained only by the physical limits of the system.

	@code
		slewMotor(&motor, 345*65535l, 1000);
		waitMotor(&motor);
	@endcode
	*/
extern void slewMotor(Motor *pMotor, int32_t s, uint16_t v);

#ifdef SCAN
/** Move a motor at a constant speed.
	This moves at a specified speed across a range. This range is defined by
	the current position and the target position. Because the specified speed
	may require ramping, the motor first backs up from the current position to
	allow a run-up to speed, and will overrun the target position as it
	deaccelerates. The function will abort any current movement, but will not
	wait for the scan movement to complete before returning.

	The scanMotor() function is only available if SCAN is defined in motor.h
	before the motor library is compiled.

	@param pMotor Pointer to a motor structure.

	@param s Target position (nanosteps, -214718111 to 2147418111).

	@param v Desired scan speed (nanosteps/tick, 1 to 65535).

	@code
		scanMotor(&motor, 345*65535l, 1000);
		waitMotor(&motor);
	@endcode
	*/
extern void scanMotor(Motor *pMotor, int32_t s, uint16_t v);
#endif

/** Stop motor movement.
	The motor is deaccelerated if necessary. The function will not wait for
	the movement to stop before returning.

	@param pMotor Pointer to a motor structure.

	@code
		stopMotor(&motor);
		waitMotor(&motor);
	@endcode
	*/
extern void stopMotor(Motor *pMotor);

/** Wait for the motor to complete movement.
	Though usually used in conjunction with the other motor movement
	functions, it can be called at any time and from tasks other than the
	one that initiated the movement. It may also be called more than once
	without intervening movements being initiated.

	@param pMotor Pointer to a motor structure.

	@code
		waitMotor(&motor);
	@endcode
	*/
extern void waitMotor(Motor *pMotor);

/** Check the motor state.
	This function might be used to implement a non-blocking wait.

	@return Movement state (true=moving, false=stopped).
	The return value is only valid for the instant the function is called. The
	motor may change state at any time, especially if this function is called
	from a task other than the one that initiates movement.

	@param pMotor Pointer to a motor structure.

	@code
		while (busyMotor(&motor))
		~do something else~
		~initiate another movement~
	@endcode
	*/
extern bool busyMotor(Motor *pMotor);

// motor output sine tables.
/** Sine table with full 256 count range.
	Range 0 to 255.
	*/
#define SineTable256 \
		{\
			0x80,0x83,0x86,0x89,0x8C,0x8F,0x92,0x95,0x98,0x9B,0x9E,0xA2,0xA5,0xA7,0xAA,0xAD,\
			0xB0,0xB3,0xB6,0xB9,0xBC,0xBE,0xC1,0xC4,0xC6,0xC9,0xCB,0xCE,0xD0,0xD3,0xD5,0xD7,\
			0xDA,0xDC,0xDE,0xE0,0xE2,0xE4,0xE6,0xE8,0xEA,0xEB,0xED,0xEE,0xF0,0xF1,0xF3,0xF4,\
			0xF5,0xF6,0xF8,0xF9,0xFA,0xFA,0xFB,0xFC,0xFD,0xFD,0xFE,0xFE,0xFE,0xFF,0xFF,0xFF,\
			0xFF,0xFF,0xFF,0xFF,0xFE,0xFE,0xFE,0xFD,0xFD,0xFC,0xFB,0xFA,0xFA,0xF9,0xF8,0xF6,\
			0xF5,0xF4,0xF3,0xF1,0xF0,0xEE,0xED,0xEB,0xEA,0xE8,0xE6,0xE4,0xE2,0xE0,0xDE,0xDC,\
			0xDA,0xD7,0xD5,0xD3,0xD0,0xCE,0xCB,0xC9,0xC6,0xC4,0xC1,0xBE,0xBC,0xB9,0xB6,0xB3,\
			0xB0,0xAD,0xAA,0xA7,0xA5,0xA2,0x9E,0x9B,0x98,0x95,0x92,0x8F,0x8C,0x89,0x86,0x83,\
			0x80,0x7C,0x79,0x76,0x73,0x70,0x6D,0x6A,0x67,0x64,0x61,0x5D,0x5A,0x58,0x55,0x52,\
			0x4F,0x4C,0x49,0x46,0x43,0x41,0x3E,0x3B,0x39,0x36,0x34,0x31,0x2F,0x2C,0x2A,0x28,\
			0x25,0x23,0x21,0x1F,0x1D,0x1B,0x19,0x17,0x15,0x14,0x12,0x11,0x0F,0x0E,0x0C,0x0B,\
			0x0A,0x09,0x07,0x06,0x05,0x05,0x04,0x03,0x02,0x02,0x01,0x01,0x01,0x00,0x00,0x00,\
			0x00,0x00,0x00,0x00,0x01,0x01,0x01,0x02,0x02,0x03,0x04,0x05,0x05,0x06,0x07,0x09,\
			0x0A,0x0B,0x0C,0x0E,0x0F,0x11,0x12,0x14,0x15,0x17,0x19,0x1B,0x1D,0x1F,0x21,0x23,\
			0x25,0x28,0x2A,0x2C,0x2F,0x31,0x34,0x36,0x39,0x3B,0x3E,0x41,0x43,0x46,0x49,0x4C,\
			0x4F,0x52,0x55,0x58,0x5A,0x5D,0x61,0x64,0x67,0x6A,0x6D,0x70,0x73,0x76,0x79,0x7C,\
		}
/** Sine table with 128 count range.
	Range 64 to 191.
	*/
#define SineTable128\
		{\
			0x80,0x81,0x83,0x84,0x86,0x87,0x89,0x8A,0x8C,0x8D,0x8F,0x90,0x92,0x93,0x95,0x96,\
			0x98,0x99,0x9B,0x9C,0x9D,0x9F,0xA0,0xA1,0xA3,0xA4,0xA5,0xA7,0xA8,0xA9,0xAA,0xAB,\
			0xAC,0xAD,0xAF,0xB0,0xB1,0xB2,0xB3,0xB3,0xB4,0xB5,0xB6,0xB7,0xB8,0xB8,0xB9,0xBA,\
			0xBA,0xBB,0xBB,0xBC,0xBC,0xBD,0xBD,0xBD,0xBE,0xBE,0xBE,0xBF,0xBF,0xBF,0xBF,0xBF,\
			0xBF,0xBF,0xBF,0xBF,0xBF,0xBF,0xBE,0xBE,0xBE,0xBD,0xBD,0xBD,0xBC,0xBC,0xBB,0xBB,\
			0xBA,0xBA,0xB9,0xB8,0xB8,0xB7,0xB6,0xB5,0xB4,0xB3,0xB3,0xB2,0xB1,0xB0,0xAF,0xAD,\
			0xAC,0xAB,0xAA,0xA9,0xA8,0xA7,0xA5,0xA4,0xA3,0xA1,0xA0,0x9F,0x9D,0x9C,0x9B,0x99,\
			0x98,0x96,0x95,0x93,0x92,0x90,0x8F,0x8D,0x8C,0x8A,0x89,0x87,0x86,0x84,0x83,0x81,\
			0x80,0x7E,0x7C,0x7B,0x79,0x78,0x76,0x75,0x73,0x72,0x70,0x6F,0x6D,0x6C,0x6A,0x69,\
			0x67,0x66,0x64,0x63,0x62,0x60,0x5F,0x5E,0x5C,0x5B,0x5A,0x58,0x57,0x56,0x55,0x54,\
			0x53,0x52,0x50,0x4F,0x4E,0x4D,0x4C,0x4C,0x4B,0x4A,0x49,0x48,0x47,0x47,0x46,0x45,\
			0x45,0x44,0x44,0x43,0x43,0x42,0x42,0x42,0x41,0x41,0x41,0x40,0x40,0x40,0x40,0x40,\
			0x40,0x40,0x40,0x40,0x40,0x40,0x41,0x41,0x41,0x42,0x42,0x42,0x43,0x43,0x44,0x44,\
			0x45,0x45,0x46,0x47,0x47,0x48,0x49,0x4A,0x4B,0x4C,0x4C,0x4D,0x4E,0x4F,0x50,0x52,\
			0x53,0x54,0x55,0x56,0x57,0x58,0x5A,0x5B,0x5C,0x5E,0x5F,0x60,0x62,0x63,0x64,0x66,\
			0x67,0x69,0x6A,0x6C,0x6D,0x6F,0x70,0x72,0x73,0x75,0x76,0x78,0x79,0x7B,0x7C,0x7E,\
		}
/** Sine table with 64 count range.
	Range 96 to 159.
	*/
#define SineTable64\
		{\
			0x80,0x80,0x81,0x82,0x83,0x83,0x84,0x85,0x86,0x86,0x87,0x88,0x89,0x89,0x8A,0x8B,\
			0x8C,0x8C,0x8D,0x8E,0x8E,0x8F,0x90,0x90,0x91,0x92,0x92,0x93,0x93,0x94,0x95,0x95,\
			0x96,0x96,0x97,0x97,0x98,0x98,0x99,0x99,0x9A,0x9A,0x9B,0x9B,0x9B,0x9C,0x9C,0x9C,\
			0x9D,0x9D,0x9D,0x9D,0x9E,0x9E,0x9E,0x9E,0x9E,0x9F,0x9F,0x9F,0x9F,0x9F,0x9F,0x9F,\
			0x9F,0x9F,0x9F,0x9F,0x9F,0x9F,0x9F,0x9F,0x9E,0x9E,0x9E,0x9E,0x9E,0x9D,0x9D,0x9D,\
			0x9D,0x9C,0x9C,0x9C,0x9B,0x9B,0x9B,0x9A,0x9A,0x99,0x99,0x98,0x98,0x97,0x97,0x96,\
			0x96,0x95,0x95,0x94,0x93,0x93,0x92,0x92,0x91,0x90,0x90,0x8F,0x8E,0x8E,0x8D,0x8C,\
			0x8C,0x8B,0x8A,0x89,0x89,0x88,0x87,0x86,0x86,0x85,0x84,0x83,0x83,0x82,0x81,0x80,\
			0x80,0x7F,0x7E,0x7D,0x7C,0x7C,0x7B,0x7A,0x79,0x79,0x78,0x77,0x76,0x76,0x75,0x74,\
			0x73,0x73,0x72,0x71,0x71,0x70,0x6F,0x6F,0x6E,0x6D,0x6D,0x6C,0x6C,0x6B,0x6A,0x6A,\
			0x69,0x69,0x68,0x68,0x67,0x67,0x66,0x66,0x65,0x65,0x64,0x64,0x64,0x63,0x63,0x63,\
			0x62,0x62,0x62,0x62,0x61,0x61,0x61,0x61,0x61,0x60,0x60,0x60,0x60,0x60,0x60,0x60,\
			0x60,0x60,0x60,0x60,0x60,0x60,0x60,0x60,0x61,0x61,0x61,0x61,0x61,0x62,0x62,0x62,\
			0x62,0x63,0x63,0x63,0x64,0x64,0x64,0x65,0x65,0x66,0x66,0x67,0x67,0x68,0x68,0x69,\
			0x69,0x6A,0x6A,0x6B,0x6C,0x6C,0x6D,0x6D,0x6E,0x6F,0x6F,0x70,0x71,0x71,0x72,0x73,\
			0x73,0x74,0x75,0x76,0x76,0x77,0x78,0x79,0x79,0x7A,0x7B,0x7C,0x7C,0x7D,0x7E,0x7F,\
		}
/** Sine table with 32 count range.
	Range 112 to 143.
	*/
#define SineTable32\
		{\
			0x80,0x80,0x80,0x81,0x81,0x81,0x82,0x82,0x83,0x83,0x83,0x84,0x84,0x84,0x85,0x85,\
			0x85,0x86,0x86,0x86,0x87,0x87,0x87,0x88,0x88,0x88,0x89,0x89,0x89,0x8A,0x8A,0x8A,\
			0x8A,0x8B,0x8B,0x8B,0x8B,0x8C,0x8C,0x8C,0x8C,0x8D,0x8D,0x8D,0x8D,0x8D,0x8E,0x8E,\
			0x8E,0x8E,0x8E,0x8E,0x8E,0x8E,0x8F,0x8F,0x8F,0x8F,0x8F,0x8F,0x8F,0x8F,0x8F,0x8F,\
			0x8F,0x8F,0x8F,0x8F,0x8F,0x8F,0x8F,0x8F,0x8F,0x8F,0x8F,0x8E,0x8E,0x8E,0x8E,0x8E,\
			0x8E,0x8E,0x8E,0x8D,0x8D,0x8D,0x8D,0x8D,0x8C,0x8C,0x8C,0x8C,0x8B,0x8B,0x8B,0x8B,\
			0x8A,0x8A,0x8A,0x8A,0x89,0x89,0x89,0x88,0x88,0x88,0x87,0x87,0x87,0x86,0x86,0x86,\
			0x85,0x85,0x85,0x84,0x84,0x84,0x83,0x83,0x83,0x82,0x82,0x81,0x81,0x81,0x80,0x80,\
			0x80,0x7F,0x7F,0x7E,0x7E,0x7E,0x7D,0x7D,0x7C,0x7C,0x7C,0x7B,0x7B,0x7B,0x7A,0x7A,\
			0x7A,0x79,0x79,0x79,0x78,0x78,0x78,0x77,0x77,0x77,0x76,0x76,0x76,0x75,0x75,0x75,\
			0x75,0x74,0x74,0x74,0x74,0x73,0x73,0x73,0x73,0x72,0x72,0x72,0x72,0x72,0x71,0x71,\
			0x71,0x71,0x71,0x71,0x71,0x71,0x70,0x70,0x70,0x70,0x70,0x70,0x70,0x70,0x70,0x70,\
			0x70,0x70,0x70,0x70,0x70,0x70,0x70,0x70,0x70,0x70,0x70,0x71,0x71,0x71,0x71,0x71,\
			0x71,0x71,0x71,0x72,0x72,0x72,0x72,0x72,0x73,0x73,0x73,0x73,0x74,0x74,0x74,0x74,\
			0x75,0x75,0x75,0x75,0x76,0x76,0x76,0x77,0x77,0x77,0x78,0x78,0x78,0x79,0x79,0x79,\
			0x7A,0x7A,0x7A,0x7B,0x7B,0x7B,0x7C,0x7C,0x7C,0x7D,0x7D,0x7E,0x7E,0x7E,0x7F,0x7F,\
		}

// pulse density/width modulator
/** Pulse modulator.
	This structure holds state information. Members of the structure are not
	to be accessed directly by the user.

	Fixed parameters within the structure are set by initPulse. The parameter
	that controls the average value is set by setPulse. The structure is used
	by the interrupt function updatePdm() to calculate the output state.

	@code
		Pulse dac0, dac1;
	@endcode
	*/
typedef struct
{
	/** Modulator accumulator.*/
	uint16_t modulo;
	/** Modulator level.*/
	uint16_t value;
	/** Port.*/
	uint8_t port;
	/** Port off and on masks.*/
	uint8_t mask0, mask1;
} Pulse;

// pulse density/width modulator routines
/** Update pulse modulator.
	This is called by a user supplied interrupt routine to update the
	modulator output. Each pulse modulator requires a separate call to the
	function.

	The function should be called early in the interrupt routine to minimize
	dither on the pulse timing. Leave interrupts disabled for the duration
	of the call, because the output to the port is not task-safe.

	@param pPulse Pointer to a pulse modulator structure.

	@code
		Pulse dac0, dac1;
		...
		void __attribute__((signal)) pulseInterrupt(void)
		{
			updatePdm(&dac0);
			updatePdm(&dac1);
			...
		}
		...
		initPulse(&dac0, PORTB, 0x40);
		initPulse(&dac1, PORTB, 0x20);
		...
		~enable pulse interrupt~
	@endcode
	*/
void updatePdm(Pulse *pPulse);

/** Initialize pulse modulator.
	This sets the fixed pulse modulator parameters, and initializes the pulse
	modulator structure. It must be called before the interrupt that calls
	updatePdm() is enabled. The default modulator value is 0.

	@param pPulse Pointer to a pulse modulator structure.

	@param port Index of the output port of the pulse modulator.

	@param mask Bit mask.
	This enables the bit to be modulated on the port. More than one bit may be
	enabled.

	@code
		Pulse dac;
		...
		initPulse(&dac, PORTB, 0x40);
	@endcode
	*/
void initPulse(Pulse *pPulse, uint8_t port, uint8_t mask);

/** Set pulse modulator level.
	This sets the average value for the pulse modulation.
	The modulator state is maintained, so that the output will not be
	incorrectly modulated by a rapidly changing set value.

	@param pPulse Pointer to a pulse modulator structure.

	@param value modulation level (0 to 65535).
	This that represents the proportion of bits which will be active low out
	of each 65536 updates to the modulator.

	@code
		setPulse(&dac, 0x4536);
	@endcode
	*/
void setPulse(Pulse *pPulse, uint16_t value);
#endif
