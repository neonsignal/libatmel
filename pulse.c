// Copyright 2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// Pulse Density/Width Modulator Driver
// include files
#include <types.h>
#include <avr/interrupt.h>
#include "motor.h"

// pulse density/width modulator routines
void updatePdm(Pulse *pPulse)
{
	// if modulo counter overflows, clear bit
	if ((pPulse->modulo += pPulse->value) < pPulse->value) // pseudo carry test
		*(uint8_t *)(uint16_t)(pPulse->port+0x20) &= pPulse->mask0;

	// else set bit
	else
		*(uint8_t *)(uint16_t)(pPulse->port+0x20) |= pPulse->mask1;
}

void initPulse(Pulse *pPulse, uint8_t port, uint8_t mask)
{
	// initialize pulse density modulator
	pPulse->modulo = 0;
	pPulse->value = 0;
	pPulse->port = port;
	pPulse->mask0 = ~mask;
	pPulse->mask1 = mask;
}

void setPulse(Pulse *pPulse, uint16_t value)
{
	// set new pulse density modulator value
	cli();
	pPulse->value = value;
	sei();
}
