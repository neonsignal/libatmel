// Copyright 1989-2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
/** @mainpage Atmel Library
	@version 1.5
	@author Glenn McIntosh
	@date 1989-2001

	@section monitor Task Monitor
		A library of C functions for controlling the real-time execution of
		multiple tasks on an embedded system. A round-robin algorithm is used for
		task scheduling. There is no prioritization, which reduces the amount of
		switching in supplier-consumer task arrangements. Each task can use no
		more than its allotted timeslice, and active tasks are run in a consistent
		order, so that maximum latency times are predictable. There is no limit to
		the number of tasks. @see monitor.h

	@section motors Motor drivers and pulse modulators
		A library of C functions for driving stepper motors and modulated digital
		outputs on an embedded system. Motors may be microstepped or single
		stepped, can be accelerated to a steady speed, and deaccelerated to rest.
		Digital outputs are pulse density modulated for use as 1 bit DACs. There
		is no limit to the number of motors or modulated digital outputs. @see
		motor.h

	@section fix32 Fixed point mathematics
		A library of C functions for mathematical operations on binary fixed point
		numbers. These functions take 0.32 bit unsigned fixed point numbers (ie,
		with values between 0.0 and 1.0), and aim to preserve full arithmetic
		precision without impacting execution speed. @see fix32.h

	@section version Version History
		@subsection V1_0. V1.0
		@li
			Added the monitor library.
		@li
			Added the motor library.
		@li
			Released 23 January 2001.

		@subsection V1_1. V1.1
		@li
			Made the makefile stand-alone.
		@li
			Changed the library name to 'libatmel.a' to comply with GNU precedents.
		@li
			Tagged the monitor lock as volatile to avoid a potential optimization
			bug.
		@li
			Changed deacceleration calculations at the end of a slew to minimize the
			number of ticks required to reach the target.
		@li
			User supplied Motor::outMotor() and switchTest() routines combined. Stop
			check done with the switch test.
		@li
			Allowed omission of the motor scanning functions during a library build.
		@li
			Combined motor forward and back routines.
		@li
			Optimized the motors stop condition test.
		@li
			Released 6 February 2001.

		@subsection V1_2. V1.2
		@li
			Added the fixed point arithmetic library.
		@li
			Released 26 March 2001.

		@subsection V1_3. V1.3
		@li
			Changed the UART output to use the Data Register Empty instead of the TX
			Complete interrupt (because the latter requires priming of the UART
			output). This change also removes the interrupt latency time between
			transmitted characters.
		@li
			Added motor busy check for status routines.
		@li
			Added motor enable/disable functionality to the movement routines.
		@li
			Incorporated comments into the header files so that an automated
			document generator could be used.
		@li
			Released 7 April 2001

		@subsection V1_4 V1.4
		@li
			Separated signalling an interrupt semaphore from an interrupts
			(setInterruptSemaphore()) and from a task (signalInterruptSemaphore()).
		@li
			Changed updateMotor() to return a reference bit pattern, and
			referenceMotor() to use bit patterns to check for the reference switch.
			This allows other motor movements (slewMotor() and scanMotor()) to
			ignore the reference switch, and makes the reference switch checking
			faster and more flexible.
		@li
			Upgrade to 01 July 2001 version of avr-gcc compiler.
		@li
			Remove interrupt protection from ouMotor() and enableMotor() routines.
		@li
			Added waitTInterruptSemaphore() and receiveTSerial() routines.
		@li
			Changed all motor movements to use a reference offset. This is because
			at the reference point, the step-microstep-nanostep offset must be
			retained (the quadstep can be zeroed). The referenceMotor() and
			zeroMotor() in earlier versions naively zeroed the current position,
			which caused the motor to jump to the nearest zero step position before
			the next movement.
		@li
			Released 16 October 2001

		@subsection V1_5 V1.5
		@li
			Added non-volatile memory functions initNvmem() and storeNvmem(). These
			allow the non-volatile memory to be updated asynchronously from a block
			of parameters.
		@li
			Changed the logic for timeout functions so that a zero timeout would
			default to checking the condition rather than timing out.
		@li
			Use outb and outw which are now supported correctly by the compiler
		@li
			Trigger functions waitTrigger() and waitTTrigger() now return the subset
			of bits that were triggered.
	*/
#ifndef _STANDARD_H
#define _STANDARD_H
// type definitions
typedef char bool;
typedef unsigned char uchar;
typedef unsigned int uint;
enum {false=0, true=1};
// integer types
#include <inttypes.h>
#endif
