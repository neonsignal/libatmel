// Copyright 1989-2000 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
#include <types.h>
#include "monitor.h"

// switch
void switchTask(void)
{
	// switch to new task
	stopSwitch();
	switchMonitor();
	startSwitch();
}
