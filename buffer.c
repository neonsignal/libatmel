// Copyright 2000 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// Circular Buffer
// include files
#include "buffer.h"

// access routines
void initBuffer(Buffer *pBuffer, uchar *buffer, uint length)
{
	// initialize
	pBuffer->length = length;
	pBuffer->head = pBuffer->tail = 0;
	pBuffer->buffer = buffer;
}

void putBuffer(Buffer *pBuffer, uchar c)
{
	// update tail pointer
	if (++(pBuffer->tail) == pBuffer->length) pBuffer->tail = 0;
	// get character
	*(pBuffer->buffer + pBuffer->tail) = c;
}

uchar getBuffer(Buffer *pBuffer)
{
	// update head pointer
	if (++(pBuffer->head) == pBuffer->length) pBuffer->head = 0;
	// get character
	return *(pBuffer->buffer + pBuffer->head);
}

void flushBuffer(Buffer *pBuffer)
{
	// reset buffer pointers
	pBuffer->head = pBuffer->tail;
}
