// Copyright 2001 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
/** @file monitor.h Task monitor library.
	A program that requires use of the monitor library will link libatmel.a
	after any object files. Any file that uses library functions will include
	monitor.h.

	Before calling any library routines, the monitor must be initialized using
	initMonitor(). The switching interrupt timerTick() should be called at
	regular intervals if time-slicing is required (so for a 6 MHz Atmel
	processor, the timerTick() could be called at around one kHz, and would take
	about 15 us, not including task switch time). Tasks are spawned by calling
	startTask().

	The monitor library is compiled in parts, so that functions which are
	not used will not be linked in. The code size of the core scheduling has
	been kept minimal (for example, around 0.5 kBytes for the Atmel processor).

	The speed of task switching is related to the number of tasks that are
	inactive, so it is better to have tasks (such as motor drivers) start and
	exit repeatedly than have them wait for an initiating event (for example, on
	a 6 MHz Atmel processor with n inactive tasks, the switch can take as much
	as (n+1) x 25 us). The time taken to start and exit a task is commensurable
	with triggering an event.

	Interrupt routines cannot call monitor routines. This is particularly
	true on processors that do not allow indivisible access to variables (for
	example, the Atmel cannot access a 16 bit number in one instruction, let
	alone increment it). Communication between interrupt functions and tasks
	requires interrupt specific routines (such as interrupt semaphores, and
	serial communications).

	Interrupts will use the stack of the active task by default. Using
	a pre-reserved stack space instead would reduce the stack requirements
	on all the tasks. However, it is still treated as running logically from
	the currently active task, so even if not using the same stack, it will
	steal time from that task and could be suspended with that task if
	interrupts are re-enabled prematurely.

	All tasks are run in a single address space, though there is task
	context memory at the base of each stack.

	Glossary of terms:
	@li
		@b active: a state of execution in which a task is ready to execute code,
		rather than waiting for an event to occur
	@li
		@b context @b memory: memory which is specific to a particular task,
		especially when a variable has different values for differnt tasks
	@li
		@b event: any object used to communicate between tasks, so that tasks may
		be suspended pending such an event
	@li
		@b latency: the time take for a task to respond to an event that it is
		waiting for
	@li
		@b monitor: an task controlling mechanism or operating system kernel which
		uses a single exclusion semaphore to control access to the kernel
	@li
		@b round-robin: a flow of control in which each task is executed in a
		consistent order (though this order may not be determinate)
	@li
		@b tasks: 'threads' of execution, which simulate having multiple processors
	@li
		@b tick: the minimum measurement of time in the system, which is used to
		measure time-slices, and divided down to provide a real-time clock
	@li
		@b time-slice: the maximum time allotted to a task during a single
		round-robin cycle of tasks
	*/

#ifndef _MONITOR_H
#define _MONITOR_H
// debug definition
//#define STACK_DEBUG
//#define RUNTIME_DEBUG
// include files
#include <types.h>
#include <buffer.h>

// context variables
/** Task context.
	Each task is allocated context memory at the base of its stack. Most
	members of this structure are for use only by the monitor library.
	*/
typedef struct Context
{
	// task switch stack
	/** Stack pointer.
		*/
	uchar *sp;
	/** Context chain.
		*/
	struct Context *nextContext, *prevContext;
	// timing information
	/** Number of timer ticks allowed for each time-slice.
		This should not be written to directly (currently it is set by
		startTask; there is no provision for changing the time after execution
		has begun). Because of tick granularity, the actual time-slice can be
		vary between runTime-1 and runTime.

		@code
			uint runTime = activeContext->runTime;
		@endcode
		*/
	uchar runTime;
#ifdef RUNTIME_DEBUG
	/** Minimum timer ticks left when the task has given up its time-slice.
		This can be subtracted from the run time to calculate the worst case for
		that task. It is only calculated if the RUNTIME_DEBUG flag is defined in
		monitor.h before the monitor library is compiled.

		@code
			uint maxUsedTime = activeContext->runTime-activeContext->freeTime;
		@endcode
		*/
	uchar freeTime;
#endif
	// errors
	/** Indication of timeout.
		This is used after returning from a wait with timeout function to
		indicate that the function timed out before the event occurred. It is
		read and cleared using resetTimeout().

		@code
			if (resetTimeout())
				~report an error~
		@endcode
		*/
	bool timeout;
	// user context
	/** Pointer to the user context structure.
		This pointer is supplied to startTask.
		*/
	void *user;
	// debug for stack overflow
#ifdef STACK_DEBUG
	/** Stack overflow guard byte.
		When this is overwritten, the task switching system calls
		the taskStackOverflow() handler.
		*/
	uchar stackGuardbits;
#endif
} Context;
/** The context of the currently executing task.
	This can be used to access context variables. The value of this
	activeContext pointer also serves to identify the task, because it does
	not change.
	*/
extern Context *activeContext;

// internal functions (used only by monitor library functions)
/** Force scheduler to switch to the next task.
	This is meant to be called only from within monitor library functions. A
	user function wanting to relinquish the remainder of its time-slice will
	call switchTask() instead. Calls to switchMonitor() assume that the
	monitor is locked by protecting the call with a startSwitch()/stopSwitch()
	sequence.

	@code
		stopSwitch();
		while ({event has not occurred})
			switchMonitor();
		startSwitch();
	@endcode
	*/
extern void switchMonitor(void);

/** Enable task switching.
	This is meant to be called only from within monitor library functions.
	Calls to startSwitch() assume that the monitor was previously locked by
	stopSwitch().
	@see switchMonitor
	*/
extern void startSwitch(void);

/** Disable task switching.
	This is meant to be called only from within monitor library functions.
	Calls to stopSwitch() assume that the monitor was previously unlocked by
	startSwitch().
	@see switchMonitor
	*/
extern void stopSwitch(void);

// timer tick
/** Set real time clock rate.
	The nominal clock rate is 10Hz. It must be called before timerTick() so
	that the monitor knows the relationship between timer ticks and clock
	ticks.

	@param hz10 number of timer ticks for each clock tick.
	The clockCnt variable increments once for every hz10 timer ticks.
	Nominally the value is the number of ticks in 0.1 seconds, or 10 times the
	tick frequency. It must be between 1 and 256 (so a 10 Hz clock must have a
	tick rate of between 10 Hz and 2.56 kHz).
	@see timerTick
	*/
extern void timerFreq(uchar hz10);

/** Update the real time clock and reschedule tasks.
	This is called periodically by a user supplied function (preferably after
	initMonitor()). It updates the real time clock clockCnt, and pre-empts
	tasks that have reached the end of their time-slice.

	Register preservation is important, because this function may cause a
	task switch. If it is called from an interrupt function, that function
	must preserve any registers not preserved by normal C code (for example,
	r0-r1,r18-r27,r30-r31,sreg for the GNU cross-compiler for the Atmel; this
	will happen automatically if the calling function is given the
	__attribute__((signal)) qualifier).

	The timerTick() function assumes that interrupts are disabled on entry to
	the function, and exits with them disabled (or re-disabled, note the
	following). If a task switch occurs, interrupts will be enabled for
	other tasks, until this task being switched is reactivated. In other
	words, the calling function should not require that this function is
	either indivisible or that it returns immediately (ie, it should not be
	called before other code in an interrupt routine).

	A task switch will not occur if the monitor is locked by stopSwitch.
	However, the timerTick() function will continue to count time-slice ticks
	even during this locked period, and any task switch that would have
	occurred during the time will be scheduled as soon as the monitor is
	unlocked by startSwitch.

	The timerTick() function will use the stack of the function that calls it.
	As with other interrupts, this would normally (but does not have to) be
	the stack of the active task.

	@code
		void __attribute__((signal)) timerInterrupt(void)
		{
			timerTick();
		}
		...
		initMonitor(2);
		timerFreq(400/10); {timer tick is 400 Hz}
		~initialize timer interrupt hardware~
		...
		uint time;
		time = getTime();
		~report real time~
	@endcode
	*/
extern void timerTick(void);

/** Get number of real-time clock ticks.
	This counter wraps around at 65536.

	@return Clock ticks.

	@see timerTick
	*/
extern uint getTime(void);

// timeout reset
/** Read and clear the timeout flag.
	The flag is set by wait with timeout functions to indicate that the
	function timed out before the event occurred. Since the wait with timeout
	functions do not clear the flag, several can be called in sequence without
	checking the timeout flag each time.

	@return Current timeout state.

	@code
		if (resetTimeout())
			~report an error~
	@endcode

	@see waitTSemaphore, receiveTMailbox, waitTState, waitTTrigger
	*/
extern bool resetTimeout(void);

// monitor initialization and task starting/exit
/** Initialize monitor library.
	Once it has been initialized (not necessarily by calling this from the C
	main function), the current control flow is considered to be the default
	task. The monitor library will be functional after this point, though
	clock events (such as delays or timeouts) cannot occur unless timerTick()
	is also being called.

	The default task is treated the same as other tasks. It may wait on
	events and signal events. It will (continue to) use the top of the main
	stack, and will place context information at DEFAULT_STACK_BASE (defined
	in machine.c. It has no initiating function, so if the C main
	function exits, it will behave just as if there was no monitor
	(typically this halts or enters an infinite loop on embedded systems).
	The default task can explicitly call exitTask() to terminate.

	@param contextSize Size of the default stack (bytes).
	The default task stack requires room for the context structure, space to
	save all of the registers in the processor, and allowance for nested
	function calls and interrupts (for example, 100 bytes would be reasonable
	for an Atmel processor).

	@param runTime Length of time slice (timer ticks).

	@code
		initMonitor(100, 2);
	@endcode
	*/
extern void initMonitor(uint contextSize, uchar runTime);

/** Spawn a new task.
	A task requires a 'main' function (which defines the start of execution
	for that thread), space for a stack, and a time-slice limit.

	A task can be started from any other task. The calling task does not
	have to wait for the new task to terminate first. Nor will starting a
	task pre-empt the time-slice of the current task.

	@param fn Task function.
	This is a C function with no parameters or return value. The function
	explicitly calls exitTask() to terminate. It must not ever return.

	@param stack Task stack.
	This is an array of bytes in RAM used for the stack and for context
	information.

	@param stackSize Size of the stack (bytes).
	Each task stack requires room for the context structure, space to save all
	of the registers in the processor, and allowance for nested function calls
	and interrupts (eg, 100 bytes would be reasonable for an Atmel processor).

	@param user User defined variable or structure.
	This would be used either to pass parameters to a task, or as storage
	space that is sensitive to the task context. It can be null.

	@param runTime Length of time slice (timer ticks).
	Tasks may give up their time-slice before their time is up (by becoming
	inactive when waiting for an event), but they are guaranteed at least this
	many ticks. Due to timer granularity, the actual time may vary between
	runTime-1 and runTime, depending on the state of other tasks (so a task
	should be given a margin of one more tick than is required).

	@code
		uchar fooStack[100];
		typedef struct
			{
			int userVar0;
			...
			} FooContext;
		FooContext fooContext;
		Semaphore fooDone;
		void fooTask(void)
		{
		FooContext *user = activeContext->user;
		int userVar0 = user->userVar0;
		while ({work still remains})
			~work away~
		exitTask(&fooDone);
		}
		...
		setSemaphore(&fooDone, 0);
		startTask(fooTask, fooStack, sizeof(fooStack), &fooContext, 2);
		waitSemaphore(&fooDone);
	@endcode
	*/
extern void startTask(void (*fn)(void), void *stack, uint stackSize, void *user, uchar runTime);

// stack checking
#ifdef STACK_DEBUG
/** Handle a stack overflow.
	This is called by the task switching system when an overflow is
	detected. The task switching will potentially crash soon after returning
	from this function. Overflows are checked using guard bits, but the
	checking only occurs if the STACK_DEBUG flag is defined in monitor.h
	before the monitor library is compiled. The offending task can be
	identified by the activeContext.

	@code
		void taskStackOverflow(void)
		{
			Context *overflowContext = activeContext;
			~throw debug interrupt or halt~
		}
	@endcode
	*/
extern void taskStackOverflow(void);
#endif

// switching
/** Relinquish time slice.
	If any other task is active, control flow will switch to one of those
	tasks. This is a way to limit processor usage by a particular task, though
	it is usually simpler to restrict the time-slice when startTask() is first
	called.

	It is unwise to use switchTask() to control the order of task execution,
	since this can lead to subtle timing bugs (nor is the task sequence
	guaranteed to be consistent between different versions of the monitor
	library).

	@code
		while (true)
			{
			~work busily for a while~
			switchTask();
			}
	@endcode
	*/
extern void switchTask(void);

// time delays
/** Suspend the task.
		The task is suspended for a fixed time interval. It will become active
		after this interval, though latency time should also be considered if
		the task switch frequency is low.

		@param time interval (clock ticks).
		This can be from 0 to 32767, nominally in tenths of seconds (that is, 0s
		to 3276.7s). A delay of zero ticks will not force a task switch; the
		switchTask() function should be used instead.

		@code
			delayTime(5);
		@endcode
	*/
extern void delayTime(uint time);

// semaphores
/** Semaphore.
	A semaphore might be used to protect a resource that is used by more
	than one task, by forcing sequential access to that resource.

	@see setSemaphore, signalSemaphore, waitSemaphore, waitTSemaphore
	*/
typedef struct
{
	/** Current count. */
	uint cnt;
} Semaphore;

/** Initialize a semaphore count.
	This is usually called before any tasks access the semaphore.

	@param pSemaphore Pointer to a semaphore structure.

	@param cnt Activated tasks.
	This is the number of tasks waiting on the semaphore that will be
	activated, or the number that can call waitSemaphore() without being
	suspended.

	@see signalSemaphore
	*/
extern void setSemaphore(Semaphore *pSemaphore, uint cnt);

/** Signal a semaphore.
	This increments the semaphore count, to activate a waiting task or allows
	another task to call waitSemaphore() without being suspended.

	@param pSemaphore Pointer to a semaphore structure.

	@code
		Semaphore foo;
		setSemaphore(&foo, 1);
		...
		void task1(void)
			...
			waitSemaphore(&foo);
			~access a resource~
			signalSemaphore(&foo);
		...
		void task2(void)
			...
			waitTSemaphore(&foo);
			if (resetTimeout())
				~report unable to access resource~
			else
			{
			~access a resource~
			signalSemaphore(&foo);
			}
	@endcode
	*/
extern void signalSemaphore(Semaphore *pSemaphore);

/** Wait on a semaphore.
	If the semaphore count is non-zero, the count will be decremented and the
	task will continue. Otherwise the task will suspend until another task
	calls signalSemaphore().

	@param pSemaphore Pointer to a semaphore structure.

	@see signalSemaphore
	*/
extern void waitSemaphore(Semaphore *pSemaphore);

/** Wait on a semaphore with a timeout.
	If the semaphore count is non-zero, the count will be decremented and the
	task will continue. Otherwise the task will suspend until another task
	calls signalSemaphore() or the time is exceeded. If the function times
	out, the timeout context flag will be set.

	@param pSemaphore Pointer to a semaphore structure.

	@param time Time out (clock ticks).
	This is from 0 to 32767, nominally in tenths of seconds (that is, 0s to
	3276.7s).

	@see signalSemaphore
	*/
extern void waitTSemaphore(Semaphore *pSemaphore, uint time);

/** Set an interrupt semaphore.
	This is used for a semaphore that is signaled by an interrupt routine.
	This increments the semaphore count, to allow a task to call
	waitInterruptSemaphore(), or another interrupt to call
	checkInterruptSemaphore(). It assumes that interrupts are disabled.
	@param pSemaphore Pointer to a semaphore structure.

	@code
		void __attribute__((signal)) signal2(void)
			...
			~provide a resource~
			setInterruptSemaphore(&foo2);
		...
		void task2(void)
			...
			waitInterruptSemaphore(&foo2);
			~access a resource~
	@endcode
	*/
extern void setInterruptSemaphore(Semaphore *pSemaphore);

/** Check an interrupt semaphore.
	This is used to check on a semaphore within an interrupt routine. If the
	semaphore count is non-zero, the count will be decremented. The calling
	task is not suspended; instead, the return value is used to decide on an
	action. It assumes that interrupts are disabled.

	@return Semaphore ready (true=non-zero, false=zero) before decrement.

	@param pSemaphore Pointer to a semaphore structure.

	@see signalInterruptSemaphore
	*/
bool checkInterruptSemaphore(Semaphore *pSemaphore);

/** Signal an Interrupt semaphore.
	This is used for a semaphore that is checked by an interrupt routine. This
	increments the semaphore count, to allow an interrupt to call
	checkInterruptSemaphore().

	@param pSemaphore Pointer to a semaphore structure.

	@code
		Semaphore foo1;
		setSemaphore(&foo1, 0);
		...
		void __attribute__((signal)) signal1(void)
			...
			if (checkInterruptSemaphore(&foo1))
			~access a resource~
		...
		void task1(void)
			...
			~provide a resource~
			signalInterruptSemaphore(&foo1);
	@endcode
	*/
extern void signalInterruptSemaphore(Semaphore *pSemaphore);

/** Wait on an interrupt semaphore.
	This is used for a semaphore that is signaled by an interrupt routine. If
	the semaphore count is non-zero, the count will be decremented and the
	task will continue. Otherwise the task will suspend until an interrupt
	routine calls setInterruptSemaphore().

	@param pSemaphore Pointer to a semaphore structure.

	@see setInterruptSemaphore
	*/
extern void waitInterruptSemaphore(Semaphore *pSemaphore);

/** Wait on a interrupt semaphore with a timeout.
	This is used for a semaphore that is signaled by an interrupt routine. If
	the semaphore count is non-zero, the count will be decremented and the
	task will continue. Otherwise the task will suspend until an interrupt
	routine calls setInterruptSemaphore() or the time is exceeded. If the
	function times out, the timeout context flag will be set.

	@param pSemaphore Pointer to a semaphore structure.

	@param time Time out (clock ticks).
	This is from 0 to 32767, nominally in tenths of seconds (that is, 0s to
	3276.7s).

	@see setInterruptSemaphore
	*/
extern void waitTInterruptSemaphore(Semaphore *pSemaphore, uint time);

// task exit
/** Terminate a task.
	This is called by a task to end its control flow. This must be done
	explicitly, because merely returning from the 'main' function (which was
	supplied to startTask) will underflow the task stack. A function can call
	exitTask() even if that function is not the 'main' function.

	Any task may exit, including the default task that called initMonitor().
	There should always be at least one task in existence (though it does
	not have to be active). If all tasks do exit, the last one will fall
	into an infinite loop at the end of the exitTask() routine (leaving the
	monitor disabled but interrupts still active).

	@param pSemaphore Task done flag. This is signalled when the task exits.
	Waiting on this semaphore guarantees that the exiting task is no longer
	using its stack, and can be restarted. It can be null.

	@see startTask
	*/
extern void exitTask(Semaphore *pSemaphore);

// mailboxes
/** Mailbox.
	@see initMailbox, sendMailbox, receiveMailbox, receiveTMailbox
	*/
typedef struct
{
	/** Access semaphore. */
	Semaphore semaphore;
	/** Buffer. */
	Buffer buffer;
} Mailbox;

/** Initialize a mailbox.
	This sets a specified size, usually called before any tasks access the
	mailbox. The mailbox buffer is supplied by the calling function.

	@param pMailbox Pointer to a mailbox structure.

	@param buffer Mailbox buffer (byte array).

	@param buflen Length of the mailbox buffer (bytes).

	@see sendMailbox
	*/
extern void initMailbox(Mailbox *pMailbox, uchar *buffer, uint bufLen);

/** Put a message into a mailbox.
	Messages are sent in first in first out order. This activates a waiting
	task or allows another task to call receiveMailbox() without being
	suspended. It does not check for overflow.

	Only one task can send to the same mailbox.

	@param pMailbox Pointer to a mailbox structure.

	@param message Message to be placed in the mailbox buffer (byte).

	@code
		Mailbox foo;
		uchar buffer[100];
		initMailbox(&foo, buffer, sizeof(buffer));
		...
		void task1(void)
			...
			uchar data;
			~generate data~
			sendMailbox(&foo, data);
			...
			~generate data~
			sendMailbox(&foo, data);
		...
		void task2(void)
			...
			uchar data;
			data = receiveMailbox(&foo, 10);
			~make use of data~
			...
			data = receiveTMailbox(&foo, 10);
			if (resetTimeout())
				~report lack of data~
			else
				~make use of data~
	@endcode
	*/
extern void sendMailbox(Mailbox *pMailbox, uchar message);

/** Wait for message from a mailbox.
	Messages are received in first in first out order. If a message is
	waiting, the task will continue. Otherwise the task will suspend until
	another task calls sendMailbox().

	Only one task can receive from the same mailbox.

	@return Message (byte).

	@param pMailbox Pointer to a mailbox structure.

	@see sendMailbox
	*/
extern uchar receiveMailbox(Mailbox *pMailbox);

/** Wait for a message from a mailbox with timeout.
	Messages are received in first in first out order. If a message is
	waiting, the task will continue. Otherwise the task will suspend until
	another task calls sendMailbox() or the time is exceeded. If the function
	times out, the timeout context flag will be set.

	Only one task can receive from the same mailbox.

	@return Message (byte).
	This will be zero if a timeout occurs.

	@param pMailbox Pointer to a mailbox structure.

	@param time timeout (clock ticks).
	This ranges from 0 to 32767, nominally in tenths of seconds (that is 0s to
	3276.7s).

	@see sendMailbox, resetTimeout
	*/
extern uchar receiveTMailbox(Mailbox *pMailbox, uint time);

// states
/** State.
	A state object might be used in a state machine, in which the same task
	both waits for the state and sets the new state, but where other tasks
	would supply error states to override the normal sequence.

	@see initState, setState, getState, waitState, waitTState, matchState, matchTState
	*/
typedef struct
{
	/** Current state. */
	uint8_t state;
	/** Current priority. */
	uint8_t priority;
} State;

/** Reset the state value.
	This is usually called before any tasks access the state value.

	@param pState Pointer to a state structure.

	@see setState
	*/
extern void initState(State *pState);

/** Set a new state.
	If a state has already been set, it will only be replaced if the priority
	of the new state is at least as high (for example, this allows error
	states to override normal states).

	@param pState Pointer to a state structure.

	@param state New state.
	This ranges from 0 to 255.

	@param priority Priority level.
	This ranges from 1 (low) to 255 (high)

	@code
		State foo;
		initState(&foo);
		...
		void task1(void)
			...
			~decide a new state~
			setState(&foo, 42, 5);
			...
			~decide a new error state~
			setState(&foo, 42, 10);
		...
		void task2(void)
			...
			uint8_t state;
			state = waitState(&foo);
			~act on the state~
			...
			state = waitTState(&foo, 10);
			if (resetTimeout())
				~report no new state~
			else
			~act on the state~
	@endcode
	*/
extern void setState(State *pState, uint8_t state, uint8_t priority);

/** Get the current state

	@return Current state.
	This will be the last state if a reset has occurred.

	@param pState Pointer to a state structure.

	@see setState
	*/
extern uint8_t getState(State *pState);

/** Wait for a new state.
	If a state has already been set, the task will reset the state and
	continue. Otherwise the task will suspend until another task calls
	setState().

	@return Current state.

	@param pState Pointer to a state structure.

	@see setState
	*/
extern uint8_t waitState(State *pState);

/** Wait for a new state with timeout.
	If the state has already been set, the task will reset the state and
	continue. Otherwise the task will suspend until another task calls
	setState() or the time is exceeded. If the function times out, the timeout
	context flag will be set.

	@return Current state.
	This will be zero if a timeout occurs.

	@param pState Pointer to a state structure.

	@param time timeout (clock ticks).
	This is from 0 to 32767, nominally in tenths of seconds (that is 0s to
	3276.7s).

	@see setState, resetTimeout
	*/
extern uint8_t waitTState(State *pState, uint time);

/** Match a state.
	If the state matches, the function will continue. Otherwise the task will
	suspend until another task sets the matching state.

	@param pState Pointer to a state structure.

	@see setState
	*/
void matchState(State *pState, uint8_t state);

/** Match a state with timeout.
	If the state matches, the function will continue. Otherwise the task will
	suspend until another task sets the matching state or the time is
	exceeded. If the function times out, the timeout context flag will be set.

	@param pState Pointer to a state structure.

	@param time timeout (clock ticks).
	This is from 0 to 32767, nominally in tenths of seconds (that is 0s to
	3276.7s).

	@see setState, resetTimeout
	*/
void matchTState(State *pState, uint8_t state, uint time);

// trigger
/** Trigger.
	@see initTrigger, changeTrigger, waitTrigger, waitTTrigger
	*/
typedef struct
{
	/** Current bit pattern. */
	uint8_t bits;
} Trigger;

/** Initialize the trigger bits.
	This is usually called before any tasks access the bits.

	@param pTrigger Pointer to a trigger structure.

	@see changeTrigger
	*/
extern void initTrigger(Trigger *pTrigger);

/** Trigger off some of the bits.
	Only the bits specified will be changed.

	@param pTrigger Pointer to a trigger structure.

	@param bits Set of 8 trigger bits.

	@code
		Trigger foo;
		initTrigger(&foo);
		...
		void task1(void)
			...
			~decide on trigger bits~
			changeTrigger(&foo, 0x42);
			...
			~decide on trigger bits~
			changeTrigger(&foo, 0x10);
		...
		void task2(void)
			...
			waitTrigger(&foo, 0x02);
			~act on the trigger~
			...
			waitTState(&foo, 0x11);
			if (resetTimeout())
				~report no trigger~
			else
			~act on the trigger~
	@endcode
	*/
extern void changeTrigger(Trigger *pTrigger, uint8_t bits);

/** Wait for trigger bits.
	The task will suspend until another task calls changeTrigger() with at
	least one matching bit.

	@return Set of bits that were triggered.

	@param pTrigger Pointer to a trigger structure.

	@param bits Set of 8 trigger bits.

	@see changeTrigger
	*/
extern uint8_t waitTrigger(Trigger *pTrigger, uint8_t bits);

/** Wait for trigger bits with timeout.
	The task will suspend until another task calls changeTrigger() with at
	least one matching bit or the time is exceeded. If the function times out,
	the timeout context flag will be set.

	@return Set of bits that were triggered.

	@param pTrigger Pointer to a trigger structure.

	@param bits Set of 8 trigger bits.

	@param time timeout (clock ticks).
	This is from 0 to 32767, nominally in tenths of seconds (that is, 0s to
	3276.7s).

	@see changeTrigger, resetTimeout
	*/
extern uint8_t waitTTrigger(Trigger *pTrigger, uint8_t bits, uint time);

// communications
/** Initialize the serial port.
	This is usually called before any tasks access the port.

	@see sendSerial
	*/
extern void initSerial(void);

/** Send a message to the serial port.
	Messages are sent in first in first out order. It does not check for
	overflow.

	Only one task can send to the serial port.

	@param message Message to be placed in the mailbox buffer (byte).

	@code
		initSerial(void);
		...
		~generate message to send~
		sendSerial(message);
		...
		uchar message;
		message = receiveSerial();
		~process message~
	@endcode
	*/
extern void sendSerial(uchar message);

/** Receive a message from the serial port.
	Messages are received in first-in first-out order. If a message is
	waiting, the task will continue and return the byte message. Otherwise the
	task will suspend until a message arrives.

	Only one task can receive from the serial port.

	@return message (byte)

	@see sendSerial
	*/
extern uchar receiveSerial(void);

/** Receive a message from the serial port with timeout.
	Messages are received in first-in first-out order. If a message is
	waiting, the task will continue and return the byte message. Otherwise the
	task will suspend until a message arrives or the time is exceeded. If the
	function times out, the timeout context flag will be set.

	Only one task can receive from the serial port.

	@param time timeout (clock ticks).
	This is from 0 to 32767, nominally in tenths of seconds (that is, 0s to
	3276.7s).

	@return message (byte)

	@see sendSerial, resetTimeout
	*/
extern uchar receiveTSerial(uint time);

// non-volatile memory access
/** Initialize non-volatile memory.
	This loads the non-volatile memory into a variable structure in RAM. It is
	usually called before any tasks access the non-volatile parameters.

	If the non-volatile memory needs to be initialized (because it has been
	erased, or the variable structure version number has changed), it is
	written using busy waits (because the structure may be too long to buffer).

	@param pNvmem pointer to the non-volatile variable structure.
	The structure members are to be set to default values before calling the
	function. The first byte member is a version number, which is compared to
	that stored previously. If it does not match, the default values are
	written to the non-volatile memory. If it does match, the values are read
	into the structure from the non-volatile memory. The version should be
	changed every time the structure is changed. A version number of -1 is
	invalid, because this is the default non-volatile memory value.

	@param nvmemSize size of the non-volatile variable structure.

	@code
		typedef struct
		{
			uchar version;
			long var1;
			...
		} Nvmem;
		Nvmem nvmem = {4, 6435};
		...
		initNvmem((uchar *) &nvmem, sizeof(nvmem));
		...
		~change var1~
		storeNvmem(&nvmem.var1, sizeof(nvmem.var1));
	@endcode
	*/
void initNvmem(uchar *pNvmem, uchar nvmemSize);

/** Store a variable into non-volatile memory.
	Variables are stored in first-in first-out order. The non-volatile memory
	can only be written to at the rate of a few hundred bytes per second, so
	variables are buffered before writing.

	The calling task does not wait for the write to complete, nor is any error
	reported on buffer overflow.

	Because the non-volatile memory has a limited number of cycles, variables
	should not necessarily be stored each time they are changed. This only
	needs to happen at critical points in the code, where the value must be
	retained after a power down.

	@param pNvVariable pointer to the variable to be stored.
	This variable must be part of the nvmem structure, because its offset
	within that structure determines its position within non-volatile memory.

	@param nvVariableSize size of the variable to be stored (bytes).

	@see initNvmem
	*/
void storeNvmem(void *pNvVariable, uchar nvVariableSize);
#endif
