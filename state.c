// Copyright 1989-2000 Glenn McIntosh
// licensed under the GNU General Public Licence version 3
// include files
#include <types.h>
#include "monitor.h"

// states
void initState(State *pState)
{
	// set state
	stopSwitch();
	pState->priority = 0;
	startSwitch();
}

void setState(State *pState, uint8_t state, uint8_t priority)
{
	// set state
	stopSwitch();
	if (priority >= pState->priority)
	{
		pState->priority = priority;
		pState->state = state;
	}
	startSwitch();
}

uint8_t getState(State *pState)
{
	uint8_t state;
	// get state
	stopSwitch();
	state = pState->state;
	startSwitch();
	return state;
}

uint8_t waitState(State *pState)
{
	uint8_t state;
	// wait for state
	stopSwitch();
	while (pState->priority == 0)
		switchMonitor();
	state = pState->state;
	pState->priority = 0;
	startSwitch();
	return state;
}

uint8_t waitTState(State *pState, uint time)
{
	uint8_t state = 0;
	// wait for state or timeout
	stopSwitch();
	time += getTime();
	while (pState->priority == 0 && (int) (time-getTime()) > 0)
		switchMonitor();
	if (pState->priority == 0)
		activeContext->timeout = true;
	else
	{
		state = pState->state;
		pState->priority = 0;
	}
	startSwitch();
	return state;
}

void matchState(State *pState, uint8_t state)
{
	// hold until state matched
	stopSwitch();
	while (pState->priority == 0 || pState->state != state)
		switchMonitor();
	startSwitch();
	return;
}

void matchTState(State *pState, uint8_t state, uint time)
{
	// wait for state or timeout
	stopSwitch();
	time += getTime();
	while ((pState->priority == 0 || pState->state != state) && (int) (time-getTime()) > 0)
		switchMonitor();
	if ((pState->priority == 0 || pState->state != state))
		activeContext->timeout = true;
	startSwitch();
	return;
}
